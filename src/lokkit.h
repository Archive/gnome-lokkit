
extern void InitGUI(int argc, char *argv[]);
extern void EndGUI(void);

struct port {
	int port;
	char *proto;
};


extern struct in_addr *parse_resolv(int *);
extern int write_firewall(int, char **, struct port *, int, int, int);
extern void flush_firewall(void);

extern char *fwscript;

extern void host_read_event(gpointer, gint, GdkInputCondition);
extern int host_connect(const char *, int);
extern void line_received(char *);
extern void line_error(void);
extern void line_eof(void);
extern void net_error(char *, ...);
extern int mail_connect(void);
extern void build_mail_dialog(void);

