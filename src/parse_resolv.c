#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <netinet/in.h>
#include <arpa/inet.h>

struct in_addr *parse_resolv(int *num)
{
	static struct in_addr addr[4];
	int adnum=0;
	char buf[512];
	
	FILE *f=fopen("/etc/resolv.conf", "r");
	if(f==NULL)
	{
		*num=0;
		return addr;
	}
	while(fgets(buf,512,f)!=NULL)
	{
		char *bp=buf;
		while(*bp && isspace(*bp))
			bp++;
		if(strncmp(bp, "nameserver", 10))
			continue;
		bp+=10;
		while(bp && isspace(*bp))
			bp++;
		if(!*bp)
			continue;
		if(!inet_aton(bp, &addr[adnum]))
			continue;
		adnum++;
		if(adnum==4)
			break;
	}
	fclose(f);
	*num = adnum;
	return addr;
}
