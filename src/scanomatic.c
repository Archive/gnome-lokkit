/*
 *	Mail scanomatic using mail-abuse.org. This will check and warn
 *	if your mail setup seems to be flawed.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdarg.h>
#include <fcntl.h>

#include <gnome.h>

#include "lokkit.h"


/*
 *	Widgets used in the mail scanning phase
 */
 
static GtkWidget *mail_dialog;
static GtkWidget *mail_label;
static GtkWidget *mail_progress;

/*
 *	The dialog box used for mail scanning
 */
 
void build_mail_dialog(void)
{
	mail_dialog = gnome_dialog_new(_("Mail relay testing"),
			GNOME_STOCK_BUTTON_CANCEL,
			NULL);
	mail_label = gtk_label_new(_("Beginning mail test."));
	
	gtk_widget_set_usize(GTK_WIDGET(GNOME_DIALOG(mail_dialog)->vbox),
		480,120);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(mail_dialog)->vbox),
		mail_label, TRUE, TRUE, 0);
	
	gtk_widget_show(mail_label);
	
	mail_progress = gtk_progress_bar_new();
	gtk_progress_set_format_string(GTK_PROGRESS(mail_progress),
		_("Mail relay check %p%% complete"));
		
	gtk_progress_set_show_text(GTK_PROGRESS(mail_progress), TRUE);
	
	gtk_widget_show(GTK_WIDGET(mail_progress));

	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(mail_dialog)->vbox),
		mail_progress, TRUE, FALSE, 0);
	
	gnome_dialog_button_connect(GNOME_DIALOG(mail_dialog),
			0, (GtkSignalFunc)gtk_exit, NULL);
	gtk_signal_connect(GTK_OBJECT(mail_dialog), "close", 
				(GtkSignalFunc)gtk_exit, NULL);
}
	
	
/*
 *	Commence the mail scan. We connect and the connector kicks off
 *	gtk event notifications to drive the line handler.
 */

int mail_connect(void)
{
	gtk_widget_show(GTK_WIDGET(mail_dialog));
	return host_connect("relay-test.mail-abuse.org", 23);
}

/*
 *	Varargs error box then quit
 */
 
void net_error(char *fmt, ...)
{
	va_list ap;
	char buf[1024];
	GtkWidget *d;
	
	va_start(ap, fmt);
	vsnprintf(buf, 1024, fmt, ap);
	va_end(ap);
	
	d=gnome_error_dialog(buf);
	gnome_dialog_run_and_close(GNOME_DIALOG(d));
}

/*
 *	State of the line input/mail-abuse machine
 */
 
static int state = 0;
static int steps = 0;

/*
 *	Accumulate multi line information blocks from mail-abuse.org
 */
 
static char *info_dialog[2] = { NULL, NULL };

static void info_dialog_flush(int n)
{
	if(info_dialog[n])
		free(info_dialog[n]);
	info_dialog[n]=NULL;
}

static void info_dialog_append(int n, char *p)
{
	if(info_dialog[n]==NULL)
		info_dialog[n] = strdup(p);
	else
	{
		char *x=realloc(info_dialog[n], strlen(info_dialog[n])+strlen(p)+1);
		if(x)
		{
			info_dialog[n]=x;
			strcat(x, p);
		}
	}
}

/*
 *	Gtk event driven callback for a line of data from libinternet
 *
 *	Scan the mail-abuse.org data and process it.
 */
 
void line_received(char *p)
{
	gfloat pos;
	
	if(state==2)
	{
		info_dialog_append(0, p);
		return;
	}
	if(strncmp(p, "System app", 10)==0)
	{
		gtk_label_set(GTK_LABEL(mail_label), _("Relay test result."));
		state=2;
		steps=18;
		info_dialog_flush(0);
		info_dialog_append(0, p);
		return;
	}
	if(strncmp(p, "Connec", 6)==0)
	{
		steps++;
		gtk_label_set(GTK_LABEL(mail_label), p);
	}
	else if(strncmp(p, "Relay test res", 14)==0)
	{
		gtk_label_set(GTK_LABEL(mail_label), _("Relay test result."));
		state=2;
		steps=18;
		info_dialog_flush(0);
	}
	else if(strncmp(p, "Relay", 5)==0)
	{
		steps++;
		if(state==0)
		{
			state=1;
			gtk_label_set(GTK_LABEL(mail_label), _("Beginning mail tests"));
		}
		info_dialog_flush(1);
	}
	else if(*p=='<' || *p=='>')
 	{
		/* Hand out the data */
		info_dialog_append(1, p);
	}
	
	pos = steps/18.0;
	if(pos>1.0)
		pos=1.0;
		
	gtk_progress_set_percentage(GTK_PROGRESS(mail_progress), pos);
}

/*
 *	An end of file from mail-abuse.org. The scan reportis done.
 */
 
void line_eof(void)
{
	GtkWidget *w;
	/* Processing done */
	gtk_widget_destroy(GTK_WIDGET(mail_dialog));
	/*
	 *	Uh oh .. is the failure string
	 */
	 
	if(info_dialog[0]==NULL)
		info_dialog_append(0, _("EOF - passed"));
	if(info_dialog[0][0]=='U')
	{
		info_dialog_append(0, _("Failed test sequence is: \n"));
		if(info_dialog[1]==NULL)
			info_dialog_append(0, _("missing!\n"));
		else
		{
			info_dialog_append(0, info_dialog[1]);
			gnome_url_show(_("http://maps.vix.com/tsi/"));
		}
	}
	w=gnome_ok_dialog(info_dialog[0]);
	gnome_dialog_run_and_close(GNOME_DIALOG(w));
	gtk_exit(0);
}

/*
 *	A sudden abort from mail-abuse.org
 */
 
void line_error(void)
{
	net_error(_("Unexpected end of file"));
}
