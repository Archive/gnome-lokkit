#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>

#include <gdk/gdk.h>
#include <config.h>
#include <gnome.h>

#include "lokkit.h"

char *fwscript	= FWSCRIPT;

static GdkImlibImage *image;
static GdkImlibImage *logo;
static GdkImlibImage *watermark;
static GtkWidget *skip, *skipend, *skipfw;
static GtkWidget *window;

static GtkWidget *page_radio[16][3];
static int pgport[16];

static int type = 0;

extern GtkWidget *Wizard(int, char *, char *, char *, char *,char *);

static struct poptOption options[] = {
	{ "fwtype", 'f', POPT_ARG_NONE, &type, 0, N_("Write Red Hat style /etc/sysconfig/ipchains rules, instead of a shell script"), NULL},
	{ NULL, '\0', 0, NULL, 0}
};

static char *intro_text = \
N_("Lokkit is a tool to provide firewalling for the average\n"
"Linux end user. Instead of having to configure firewall\n"
"rules the Lokkit program asks a small number of simple\n"
"questions and writes a firewall rule set for you.\n\n"
"Lokkit is not designed to configure arbitary firewalls.\n"
"To make it simple to understand it is solely designed to\n"
"handle typical dialup user and cable modem setups.\n\n"
"It is not the equal of an expert firewall designer.");

static char *final_text = \
N_("Lokkit is about to activate firewalling on this system.\n"
"It is strongly recommended you only activate the \n"
"firewalling facilities while you have direct access \n"
"to the machine, as you may not be able to get access \n"
"to deactivate them remotely.\n");

static int has_device(char *name)
{
	FILE *f=fopen("/proc/net/dev", "r");
	char buf[512];
	int l=strlen(name);
	char *bp;
	
	while(fgets(buf,512,f)!=NULL)
	{
		bp=buf;
		while(*bp==' ')
			bp++;
		if(strncmp(bp, name, l)==0 && bp[l]==':')
		{
			fclose(f);
			return 1;
		}
	}
	fclose(f);
	return 0;
}


static GtkWidget *InvokeWizard(char *title)
{
	GtkWidget *page;
	GnomeCanvasItem *item;
	GdkColor color;
	
	page = gnome_druid_page_standard_new_with_vals(title, logo);
	color.red=71*255;
	color.green=107*255;
	color.blue=179*255;
	gnome_druid_page_standard_set_bg_color(GNOME_DRUID_PAGE_STANDARD(page),&color);	
	item = gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(GNOME_DRUID_PAGE_STANDARD(page)->canvas)),
		gnome_canvas_image_get_type(),
		"image", image,
		"x", 0.0,
		"y", 0.0,
		"anchor", GTK_ANCHOR_NORTH_WEST,
		"width", (gfloat)462,
		"height", (gfloat)67,
		NULL);
		
	gnome_canvas_item_raise_to_top(GNOME_DRUID_PAGE_STANDARD(page)->title_item);
	return page;
}	

static GtkWidget *IntroPage(void)
{
	GnomeCanvasItem *item;
	GtkWidget *page = gnome_druid_page_start_new_with_vals (_("Configure Firewalling"), _(intro_text), logo, watermark);
	GdkColor color;
	
	color.red = 71*255;
	color.green = 107*255;
	color.blue = 179*255;
	gnome_druid_page_start_set_bg_color (GNOME_DRUID_PAGE_START (page),
					     &color);

	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (GNOME_DRUID_PAGE_START (page)->canvas)),
				      gnome_canvas_image_get_type (),
				      "image", image,
				      "x", 0.0,
				      "y", 0.0,
				      "anchor", GTK_ANCHOR_NORTH_WEST,
				      "width", (gfloat) 462,
				      "height", (gfloat) 67,
				      NULL);
	gnome_canvas_item_raise_to_top(item);
	gnome_canvas_item_raise_to_top (GNOME_DRUID_PAGE_START (page)->title_item);
	return page;

}

static GtkWidget *FinalPage(void)
{
	GtkWidget *page;
	GdkColor color;
	GnomeCanvasItem *item;

	page = gnome_druid_page_finish_new_with_vals (_("Activate The Firewall"), _(final_text), logo, watermark);
	color.red = 71*255;
	color.green = 107*255;
	color.blue = 179*255;
	gnome_druid_page_finish_set_bg_color (GNOME_DRUID_PAGE_FINISH (page),
					      &color);

	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (GNOME_DRUID_PAGE_FINISH (page)->canvas)),
				      gnome_canvas_image_get_type (),
				      "image", image,
				      "x", 0.0,
				      "y", 0.0,
				      "anchor", GTK_ANCHOR_NORTH_WEST,
				      "width", (gfloat) 462,
				      "height", (gfloat) 67,
				      NULL);
	gnome_canvas_item_raise_to_top (GNOME_DRUID_PAGE_FINISH (page)->title_item);
	return page;
}

/*
 *	Courtesy of #gnome. Turn a widget white background.
 */
 
static void turn_white(GtkWidget *w)
{
	GtkStyle *ns = gtk_style_copy(w->style);
	gtk_style_ref(ns);
	ns->bg[GTK_STATE_NORMAL] = ns->white;
	ns->fg[GTK_STATE_NORMAL] = ns->black;
	gtk_widget_set_style(w, ns);
	gtk_style_unref(ns);
}

static void turn_blue(GtkWidget *w)
{
	GdkColor color;
	GtkStyle *ns = gtk_style_copy(w->style);
	gtk_style_ref(ns);
	color.red = 71*255;
	color.green = 107*255;
	color.blue = 179*255;
	ns->bg[GTK_STATE_NORMAL] = color;
	gtk_widget_set_style(w, ns);
	gtk_style_unref(ns);
}

GtkWidget *Wizard(int pg, char *title, char *body, char *o1, char *o2, char *o3)
{
	GtkWidget *page;
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *align;
	GtkWidget *vbox2;
	GtkWidget *hbox;
	GnomeCanvasItem *item;
	GtkWidget *canvas;
	GdkColor color;	
	GSList *rgrp = NULL;

	color.red = 71*255;
	color.green = 107*255;
	color.blue = 179*255;
	

	/*
	 *	This is ugly, pokes around in the innards of the druid objects
	 *	if it breaks please complain to the author of gnome_druid not
	 *	me. I really don't care. 
	 *			- Alan
	 */
	 
	page = InvokeWizard(title);
	turn_white(page);

	gtk_drawing_area_size(GTK_DRAWING_AREA(GNOME_DRUID_PAGE_STANDARD(page)->side_bar),0,0);
	gtk_drawing_area_size(GTK_DRAWING_AREA(GNOME_DRUID_PAGE_STANDARD(page)->bottom_bar),0,0);
	vbox = GNOME_DRUID_PAGE_STANDARD(page)->vbox;
	gtk_box_set_spacing(GTK_BOX(vbox),0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 0);
	hbox = gtk_hbox_new(FALSE, 0);
	canvas = gnome_canvas_new();
	gtk_widget_set_usize(canvas, 159, 254);
	turn_blue(canvas);
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), 0.0, 0.0, 158.0, 254.0);
	
	item = gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_image_get_type(),
		"image", watermark,
		"x", 0.0,
		"y", 3.0,
		"anchor", GTK_ANCHOR_NORTH_WEST,
		"width", (gfloat)158,
		"height", (gfloat)254,
		NULL);
		
	item = gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_rect_get_type(),
		"fill_color_rgba", (71<<24)|(107<<16)|(179<<8),
		NULL);

	gtk_box_pack_start(GTK_BOX(hbox), canvas, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
	
	/*
	 *	We now resume our scheduled programming
	 */
	 
	vbox = gtk_vbox_new(TRUE, 0);

	gtk_box_set_spacing(GTK_BOX(vbox),0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);

	align = gtk_alignment_new (0.5, 0.5, 0, 0);
	gtk_box_pack_start (GTK_BOX (vbox), align, FALSE, TRUE, 0);
	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (align), vbox2);
	
	label = gtk_label_new(body);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
	gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start(GTK_BOX(vbox2),label, TRUE, TRUE, 0);
	
	if(o1)
	{
		page_radio[pg][0]=gtk_radio_button_new_with_label(NULL, o1);
		rgrp = gtk_radio_button_group(GTK_RADIO_BUTTON(page_radio[pg][0]));
		gtk_box_pack_start(GTK_BOX(vbox2), page_radio[pg][0], TRUE, TRUE, 0);
	}
	if(o2)
	{
		page_radio[pg][1]=gtk_radio_button_new_with_label(rgrp, o2);
		rgrp = gtk_radio_button_group(GTK_RADIO_BUTTON(page_radio[pg][1]));
		gtk_box_pack_start(GTK_BOX(vbox2), page_radio[pg][1], TRUE, TRUE, 0);
	}
	if(o3)
	{
		page_radio[pg][2]=gtk_radio_button_new_with_label(rgrp, o3);
		gtk_box_pack_start(GTK_BOX(vbox2), page_radio[pg][2], TRUE, TRUE, 0);
	}
	gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
	return page;
}

static GtkWidget *Service(int pg, char *name, int port, char *data, char *yes, char *no)
{
	int s=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(s!=-1)
	{
		struct sockaddr_in sin;
		sin.sin_port = htons(port);
		sin.sin_addr.s_addr = htonl(0x7F000001);
		sin.sin_family = AF_INET;
		
		fcntl(s, F_SETFL, FNDELAY);
		
		if(connect(s, (struct sockaddr *)&sin, sizeof(sin))==-1)
		{
			if(errno==EINPROGRESS)
			{
				fd_set fds;
				struct timeval tv;
				FD_ZERO(&fds);
				FD_SET(s,&fds);
				tv.tv_sec=1;
				tv.tv_usec=0;
				if(select(s+1, &fds, &fds, NULL, &tv)<1)
				{
					close(s);
					return NULL;
				}
			}
		}
		close(s);
	}
	pgport[pg] = port;
	return Wizard(pg, name, data, yes, no, NULL);
}

static void cancel(GtkWidget *widget, gpointer window)
{
	gtk_widget_destroy(gtk_widget_get_toplevel(widget));
	gtk_exit(0);
}

static void service_skip(GtkWidget *widget, gpointer d)
{
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[4][1])))
		gnome_druid_set_page(GNOME_DRUID(d), GNOME_DRUID_PAGE(skip));
}

static void firewall_skip(GtkWidget *widget, gpointer d)
{
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[0][2])))
		gnome_druid_set_page(GNOME_DRUID(d), GNOME_DRUID_PAGE(skip));

}

static void service_skip_end(GtkWidget *widget, gpointer d)
{
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[0][2])))
		gnome_druid_set_page(GNOME_DRUID(d), GNOME_DRUID_PAGE(skipfw));
	else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[4][1])))
		gnome_druid_set_page(GNOME_DRUID(d), GNOME_DRUID_PAGE(skipend));
}

static void mail_check(int rep, gpointer d)
{
	if(rep==1)
		gtk_exit(0);
}
	
static void apply_firewall(GtkWidget *whocares, gpointer notme)
{
	struct port *ports;
	int i;
	int p=0;
	int policy = 1;
	/* This is used, the gtk header doesnt mark the func volatile when using gcc */
	char **devs;
	GtkWidget *d;
	int dhcp=0;
	int antispamify=0;
	
	/*
	 *	GUI stuff is done. Now set everything up
	 */

	ports = malloc(16 * sizeof (struct port));
	memset(ports, '\0', 16 * sizeof(struct port));
	
	devs = malloc(3 * sizeof(char *));
	memset(devs, '\0', 3 * sizeof(char *));
		
	/*
	 *	Scan services
	 */

	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[4][0])))
	{
		for(i=0;i<16;i++)
		{
			if(pgport[i] && page_radio[i][0])
			{
				if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[i][0])))
				{
					if(pgport[i]==25)
						antispamify=1;
					ports[p++].port = pgport[i];
					ports[p-1].proto = strdup("tcp");
				}
			}
		}
	}

	/*
	 *	DHCP
	 */	

	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[3][0])))
		dhcp=1;

	/*
	 *	Global policy
	 */	
	
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[0][0])))
		policy=0;
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[0][1])))
		policy=1;
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[0][2])))
		flush_firewall();
	else
	{		
		/*
		 *	Ethernet
		 */
		 
		if(page_radio[1][0])
		{
			if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[1][0])))
			        devs[0] = strdup("eth0");
		}
	
		if(page_radio[2][0])
		{
			if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[2][0])))
			{
				if (devs[0])
				  devs[1] = strdup("eth1");
				else
				  devs[0] = strdup("eth1");
			}
		}
		if(write_firewall(policy, devs, ports, dhcp, type, 1)==-1)
		{
			GtkWidget *d;
			/* Error dialog */
			d=gnome_error_dialog(_("Unable to write firewall rules"));
			gnome_dialog_run_and_close(GNOME_DIALOG(d));
			gtk_exit(1);
		}
	}
	for (i=0; ports[i].port; i++)
	  if (ports[i].proto) free(ports[i].proto);
	free(ports);
	for (i=0; devs[i]; i++)
	  free(devs[i]);
	free(devs);
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(page_radio[0][2])) ||
		antispamify)
	{
		d=gnome_ok_cancel_dialog(_("Mail services are enabled. Checking for relaying"),
			(GnomeReplyCallback)mail_check, d);
		gnome_dialog_run_and_close(GNOME_DIALOG(d));
		gtk_widget_destroy(gtk_widget_get_toplevel(window));
		mail_connect();
	}
	else
		gtk_exit(0);
}

int main(int argc, char *argv[])
{
	GtkWidget *druid, *page;
	FILE *fw;
	
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);
	gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv,
				   options, 0, NULL);
	image = gdk_imlib_load_image("/usr/share/pixmaps/rp3/rp3-top.png");
	logo = gdk_imlib_load_image("/usr/share/pixmaps/redhat/shadowman-48.png");
	watermark = gdk_imlib_load_image ("/usr/share/pixmaps/rp3/rp3-left.png");
	if (type) {
		fwscript = strdup("/etc/sysconfig/ipchains");
	}

	if(geteuid())
	{
		GtkWidget *d=gnome_error_dialog(_("You need to be the superuser to use this application."));
		gnome_dialog_run_and_close(GNOME_DIALOG(d));
		gtk_exit(0);
	}
	if((fw=fopen(fwscript, "r")) != NULL) 
	{	
		int ret;
		
		GtkWidget *d=gnome_message_box_new(
				_("You are about to override your old firewall configuration.\n Are you sure you want to continue?\n"), 
				GNOME_MESSAGE_BOX_WARNING,
				GNOME_STOCK_BUTTON_YES,
				GNOME_STOCK_BUTTON_NO,
				NULL);
		fclose(fw);
		ret = gnome_dialog_run (GNOME_DIALOG (d));
		switch (ret) {
			case 0: break;  /* YES */
			case 1: exit(1); /* NO */
			default: exit(1); /* User closed d with wm */
		}
	}
	window = gtk_window_new(GTK_WINDOW_DIALOG);
	gtk_window_set_title(GTK_WINDOW(window), _("Configure Firewalling"));
	druid = gnome_druid_new();
	gtk_container_add(GTK_CONTAINER(window), druid);
	
	page=IntroPage();	
	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));
	gnome_druid_set_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));

	page = Wizard(0, _("Basic Configuration"),
		_("Lokkit can set up either a low or high security "
		"configuration. A high security configuration blocks all "
		"incoming accesses but requires you use specific ftp "
		"configurations and blocks irc dcc. The lower security "
		"option blocks system services but is not so intrusive. "),
		_("High Security - more intrusive"),
		_("Low Security  - less intrusive"),
		_("Disable Firewall - no protection"));

	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));
	gtk_signal_connect(GTK_OBJECT(page), "next", GTK_SIGNAL_FUNC(firewall_skip), druid);
	skipfw = page;

	if(has_device("eth0"))
	{
		page = Wizard(1, _("Local Hosts"),
			_("You seem to have an ethernet (eth0) attached to your machine. "
			"Shall I trust the hosts that are connected via this card? "
			"If this connection is your link to the internet (eg cable "
			"modem) you should say no."),
			_("   Yes   "),
			_("   No    "),
			NULL);
		gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));
	}
	if(has_device("eth1"))
	{
		page = Wizard(2, _("Local Hosts"),
				_("You seem to have a second ethernet (eth1) attached to your machine. "
				"Shall I trust the hosts that are connected via this card? "
				"If this connection is your link to the internet (eg cable "
				"modem) you should say no."),
				_("   Yes   "),
				_("   No    "),
				NULL);
		gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));
	}

	page = Wizard(3, _("DHCP"),
			_("Are you using DHCP on any of your interfaces? If you are "
			  "then say yes and the firewall rules will allow DHCP "
			  "clients to negotiate addresses with your provider."),
			_("   Yes   "),
			_("   No    "),
			NULL);
		gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));

	page = Wizard(4, _("Services"),
			_("You can enable access to some of the incoming services "
			"on your machine. Before doing so you should check you "
			"are up to date with any errata from your Linux vendor.\n"
			"Do you wish to select services to enable.\n"),
			_("   Yes   "),
			_("   No    "),
			NULL);
	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));
	gtk_signal_connect(GTK_OBJECT(page), "next", GTK_SIGNAL_FUNC(service_skip), druid);
	skipend=page;
	

	page = Service(5, _("Web Server"), 80, 
		_("This allows people to access and view the web server on "
		"your computer. You do not need to enable this to use the "
		"web server directly from the machine itself for things "
		"like developing pages."),
			_("   Yes   "),
			_("   No    ")
		);	
		
	if(page)
		gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));
	
	page = Service(6, _("Incoming Mail"), 25,
		_("Allow incoming SMTP mail delivery. You do not need to enable "
		"this if you collect your mail from your ISP's server by POP3 "
		"or IMAP, or if you use a tool such as fetchmail."),
			_("   Yes   "),
			_("   No    ")
		);

	if(page)
		gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));

	page = Service(7, _("Secure Shell"), 22,
		_("Allow incoming secure shell access. This is an encrypted "
		"way to talk to your computer over the internet. You may "
		"also wish to read the ssh documentation and set up a list "
		"of hosts permitted to connect."),
			_("   Yes   "),
			_("   No    ")
		);

	if(page)
		gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));

	page = Service(8, _("Telnet"), 23, 
		_("Allow incoming telnet access. This allows access to the "
		"command line shell of the machine from outside, if you "
		"have a username and password. It is not encrypted and can "
		"therefore be spied upon. If possible avoid enabling this."),
			_("   Yes   "),
			_("   No    ")
		);

	if(page)
		gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));
		
	skip=page=FinalPage();
	gnome_druid_append_page(GNOME_DRUID(druid), GNOME_DRUID_PAGE(page));

	gtk_signal_connect_after(GTK_OBJECT(page), "back", GTK_SIGNAL_FUNC(service_skip_end), druid);
	gtk_signal_connect_after(GTK_OBJECT(page), "finish", GTK_SIGNAL_FUNC(apply_firewall), NULL);
	gtk_widget_show_all(window);
	gtk_signal_connect(GTK_OBJECT(druid), "cancel", (GtkSignalFunc)cancel, window);
	gtk_signal_connect(GTK_OBJECT (window), "delete_event", GTK_SIGNAL_FUNC (cancel), NULL);

	build_mail_dialog();
	gtk_main();
	exit(0);
}

