
#include <dirent.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>

#include <libintl.h>
#include <locale.h>
#define N_(String) gettext(String)

#include <netinet/in.h>

#include <config.h>
#include <newt.h>
#include <popt.h>

#include "lokkit.h"

char *fwscript    = FWSCRIPT;

static int high = 0, medium = 0, disabled = 0, dhcp = 0;
static int numports = 0, numdevs = 0;
static char **devs = NULL;
static struct port *ports = NULL;


char **getNetworkDevices(int *len) {
	int num = 0;
	DIR *dir;
	char **ret = malloc(10*sizeof(char *));
	struct dirent *dent;
	
	dir = opendir("/etc/sysconfig/network-scripts");
	if (dir) {
		while ((dent = readdir(dir))) {
			if (!strncmp(dent->d_name,"ifcfg-",6) && 
			    !strstr(dent->d_name,":") &&
			    strcmp(dent->d_name,"ifcfg-lo")) {
				if (num > 10)

				  ret = realloc(ret, (num+1) * sizeof(char *));
				ret[num] = strdup(dent->d_name+6);
				num++;
			}
		}
		closedir(dir);
	}
	*len = num;
	if (ret) {
		qsort((void *)ret, num, sizeof(char *), 
		      (int (*)(const void *, const void *))strcmp);
	}
	if (!num)
	  return NULL;
	else
	  return ret;
}

void helpCallback(newtComponent co, void * tag) {
	char *helpnormal = N_("\n"
			     "Firewall Configuration\n"
			     "\n"
			     "   Red Hat Linux also offers you firewall protection\n"
			     "   for enhanced system security. A firewall sits\n"
			     "   between your computer and the network, and\n"
			     "   determines which resources on your computer remote\n"
			     "   users on the network are able to access. A\n"
			     "   properly configured firewall can greatly increase\n"
			     "   the out-of-the-box security of your system.\n"
			     "\n"
			     "   Choose the appropriate security level for your\n"
			     "   system.\n"
			     "\n"
			     "   High Security -- By choosing High Security, your\n"
			     "   system will not accept connections that are not\n"
			     "   explicitly defined by you. By default, only the\n"
			     "   following connections are allowed:\n"
			     "\n"
			     "     * DNS replies\n"
			     "     * DHCP -- so any network interfaces that use\n"
			     "       DHCP can be properly configured.\n"
			     "\n"
			     "   Using this High Security will not allow the\n"
			     "   following:\n"
			     "\n"
			     "     * Active mode FTP (Passive mode FTP, used by\n"
			     "       default in most clients, should work fine.)\n"
			     "     * IRC DCC file transfers\n"
			     "     * RealAudio(tm)\n"
			     "     * Remote X Window System clients\n"
			     "\n"
			     "   If you are connecting your system to the Internet,\n"
			     "   but do not plan to run a server, this is the\n"
			     "   safest choice. If additional services are needed,\n"
			     "   you can choose Customize to allow specific\n"
			     "   services through the firewall.\n"
			     "\n"
			     "   Medium Security -- Choosing Medium Security will\n"
			     "   not allow your system to have access to certain\n"
			     "   resources. By default, access to the following\n"
			     "   resources are not allowed:\n"
			     "\n"
			     "     * ports lower than 1023 -- these are the\n"
			     "       standard reserved ports, used by most system\n"
			     "       services, such as FTP, SSH, telnet, and HTTP.\n"
			     "     * NFS server port (2049)\n"
			     "     * the local X Window System display for remote X\n"
			     "       clients\n"
			     "     * the X Font server port (This is disabled by\n"
			     "       default in the font server.)\n"
			     "\n"
			     "   If you want to allow resources such as\n"
			     "   RealAudio(tm), while still blocking access to\n"
			     "   normal system services, choose Medium Security.\n"
			     "   You can choose Customize to allow specific\n"
			     "   services through the firewall.\n"
			     "\n"
			     "   No Firewall -- No firewall allows complete access\n"
			     "   and does no security checking. It is recommended\n"
			     "   that this only be selected if you are running on a\n"
			     "   trusted network (not the Internet), or if you plan\n"
			     "   to do more detailed firewall configuration later.\n"
			     "\n"
			     "   Choose Customize to add trusted devices or to\n"
			     "   allow additional incoming interfaces.\n");
	char *helpcustom = N_("\n"
			     "Firewall Customization\n"
			     "\n"
			     "   Choose which trusted devices and incoming services\n"
			     "   should be allowed for your network security\n"
			     "   settings.\n"
			     "\n"
			     "   Trusted Devices -- Checking these for any of your\n"
			     "   devices allows all traffic coming from that device\n"
			     "   to be allowed. For example, if you are running a\n"
			     "   local network, but are connecting to the Internet\n"
			     "   via a PPP dialup, you could check that eth0 is\n"
			     "   trusted to allow any traffic coming from your\n"
			     "   local network.\n"
			     "\n"
			     "   It is not recommended to enable this for devices\n"
			     "   that are connected to public networks, such as the\n"
			     "   Internet.\n"
			     "\n"
			     "   Allow Incoming -- Enabling these options allow the\n"
			     "   specified services to pass through the firewall.\n"
			     "   Note, during a workstation-class installation, the\n"
			     "   majority of these services are not present on the\n"
			     "   system.\n"
			     "\n"
			     "     * DHCP -- This allows DHCP queries and replies,\n"
			     "       and allows any network interfaces that use\n"
			     "       DHCP to determine their IP address. DHCP is\n"
			     "       normally enabled.\n"
			     "     * SSH -- Secure Shell (SSH) is a protocol for\n"
			     "       logging into and executing commands on remote\n"
			     "       machines. It provides secure encrypted\n"
			     "       communications. If you plan on accessing your\n"
			     "       machine remotely via SSH over a firewalled\n"
			     "       interface, enable this option. You need the\n"
			     "       openssh-server package installed for this\n"
			     "       option to be useful.\n"
			     "     * Telnet -- Telnet is a protocol for logging\n"
			     "       into remote machines. It is unencrypted, and\n"
			     "       provides little security from network snooping\n"
			     "       attacks. Enabling telnet is not recommended.\n"
			     "       You need the telnet-server package installed\n"
			     "       for this option to be useful.\n"
			     "     * WWW (HTTP) -- HTTP is the protocol used by\n"
			     "       Apache to serve Web pages. If you plan on\n"
			     "       making your Web server publicly available,\n"
			     "       enable this option. This option is not\n"
			     "       required for viewing pages locally or\n"
			     "       developing Web pages. You need the Apache\n"
			     "       package installed for this option to be\n"
			     "       useful.\n"
			     "     * Mail (SMTP) -- This allows incoming SMTP mail\n"
			     "       delivery. If you need to allow remote hosts to\n"
			     "       connect directly to your machine to deliver\n"
			     "       mail, enable this option. You do not need to\n"
			     "       enable this if you collect your mail from your\n"
			     "       ISP's server by POP3 or IMAP, or if you use a\n"
			     "       tool such as fetchmail. Note that an\n"
			     "       improperly configured SMTP server can allow\n"
			     "       remote machines to use your server to send\n"
			     "       spam.\n"
			     "     * FTP -- FTP is a protocol used for remote file\n"
			     "       transfer. If you plan on making your FTP\n"
			     "       server publicly available, enable this option.\n"
			     "       You need the wu-ftpd (and possibly anonftp)\n"
			     "       packages installed for this option to be\n"
			     "       useful.\n"
			     "     * Other ports -- You can specify that other\n"
			     "       ports not listed here be allowed through the\n"
			     "       firewall. The format to use is\n"
			     "       'port:protocol'. For example, if you wanted to\n"
			     "       allow IMAP access through your firewall, you\n"
			     "       can specify 'imap:tcp'. You can also specify\n"
			     "       numeric ports explicitly; to allow UDP packets\n"
			     "       on port 1234 through, specify '1234:udp'. To\n"
			     "       specify multiple ports, separate them by\n"
			     "       commas.\n");

	char *texttag = (char *)tag;
	if (!strcmp(texttag,"help")) {
		newtWinMessage(N_("Firewall Configuration"), N_("Ok"), helpnormal);
	} else {
		newtWinMessage(N_("Firewall Customization"), N_("Ok"), helpcustom);
	}
}

void add_dev(char *name) {
	int x, found = 0;
				
	for (x = 0; devs && devs[x]; x++)
	  if (!strcmp(devs[x], name))
	    found = 1;
	if (!found) {
		devs = realloc(devs, (numdevs + 2) *
			       sizeof(char *));
		devs[numdevs] = name;
		devs[numdevs+1] = 0;
		numdevs++;
	}
}

void add_port(char *name) {
	char *tmp;
	char *protospec;
	struct servent *service;
	int x, found = 0, port = 0;
			
	if ((tmp = strstr(name,":"))) {
		*tmp = '\0';
		tmp++;
		protospec = tmp;
	} else {
		protospec = "tcp";
	}
	port = strtol(name, &tmp, 10);
	if (tmp) {
		service = getservbyname(name, protospec);
		if (service)
		  port = ntohs(service->s_port);
	}
	if (port) {
		for (x = 0; ports && ports[x].port; x++)
		  if (ports[x].port == port && !strcmp(ports[x].proto, protospec))
		    found = 1;
		if (!found) {
			ports = realloc(ports, (numports+2) *
					sizeof(struct port));
			ports[numports].port = port;
			ports[numports].proto = protospec;
			ports[numports+1].port = 0;
			numports++;
		}
	}
}

char *add_portstr(char *orig, struct port *port) {
	char tmp[256];
	char *ret;
	struct servent *service;
	
	memset(tmp,'\0', 256);
	service = getservbyport(htons(port->port), port->proto);
	if (service) {
		snprintf(tmp, 255, "%s:%s", service->s_name, port->proto);
	} else {
		snprintf(tmp, 255, "%d:%s", port->port, port->proto);
	}
	ret = calloc((orig ? strlen(orig) : 0) + strlen(ret) + 2, sizeof(char));
	if (orig)
	  sprintf(ret, "%s %s", orig, tmp);
	else
	  sprintf(ret, "%s", tmp);
	return ret;
}

struct devcb {
	char *name;
	newtComponent comp;
	char val;
};

void runInterface() {
	newtComponent b_ok, b_custom, b_cancel, textbox;
	newtComponent paranoid_rb, simple_rb, disabled_rb;
	newtComponent other, answer;
	struct devcb *dev_cbs;
	char *portlist = NULL;
	char **devices = NULL;
	char *porttext = NULL;
	char roottext[80];
	int currentRow = 0, numdevices = 0, i, j;
	char dhcp_state, ssh_state, telnet_state, smtp_state, http_state, ftp_state;
	int ssh = 0, telnet = 0, smtp = 0, http = 0, ftp = 0;
	newtGrid bb, toplevel, poplevel;
	newtGrid smallGrid, bigGrid, typeGrid, devGrid, portGrid, oGrid;
	
	bb = newtButtonBar (N_("OK"), &b_ok, N_("Customize"), &b_custom, N_("Cancel"), &b_cancel, NULL);
	
	toplevel = newtCreateGrid(1, 5);
	textbox = newtTextboxReflowed(-1, -1, N_("A firewall protects against unauthorized "
		 "network intrusions. High security blocks all "
		 "incoming accesses. Medium blocks access "
		 "to system services (such as telnet or printing), "
		 "but allows other connections. No firewall allows "
		 "all connections and is not recommended. "), 50, 5, 10, 0);
						
	smallGrid = newtCreateGrid(2,1);
	
	bigGrid = newtCreateGrid(2,15);
	
	typeGrid = newtCreateGrid(3,2);

	newtGridSetField (smallGrid, 0, 0, NEWT_GRID_COMPONENT,
			  newtLabel(-1, -1,N_("Security Level:")),
			  0, 0, 0, 1, NEWT_ANCHOR_LEFT, 0);
	paranoid_rb = newtRadiobutton(-1, -1, N_("High"), high, NULL);
	newtGridSetField (typeGrid, 0, 0, NEWT_GRID_COMPONENT, paranoid_rb,
			  0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	simple_rb = newtRadiobutton(-1, -1, N_("Medium"), medium, paranoid_rb);
	newtGridSetField (typeGrid, 1, 0, NEWT_GRID_COMPONENT, simple_rb,
			  0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	disabled_rb = newtRadiobutton(-1, -1, N_("No firewall"), disabled, simple_rb);
	newtGridSetField (typeGrid, 2,0, NEWT_GRID_COMPONENT, disabled_rb,
			  0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	
	newtGridSetField (smallGrid, 1, 0, NEWT_GRID_SUBGRID, typeGrid,
			  1, 0, 0, 1, NEWT_ANCHOR_LEFT, 0);
	
	currentRow = 0;
	devices = getNetworkDevices(&numdevices);
	if (devices) {
		newtComponent cb;
		int curcol, currow, rows, cols;
		
		if (numdevices > 4) {
			rows = numdevices % 4 + 1;
			cols = 4;
		} else {
			cols = numdevices;
			rows = 1;
		}

		devGrid = newtCreateGrid(cols, rows);

		newtGridSetField(bigGrid, 0, currentRow, NEWT_GRID_COMPONENT,
				 newtLabel(-1, -1, N_("Trusted Devices:")),
				 0, 0, 0, 1, NEWT_ANCHOR_LEFT, 0);
		curcol = 0;
		currow = 0;
		dev_cbs = malloc(numdevices * sizeof(struct devcb));
		for (i=0; i < numdevices; i++) {
			for (j = 0; devs && devs[j] ; j++)
			  if (!strcmp(devs[j], devices[i])) {
				  cb = newtCheckbox (-1, -1, devices[i], '*', NULL, &dev_cbs[i].val);
				  break;
			  }
			if (!devs || !devs[j])
			  cb = newtCheckbox (-1, -1, devices[i], ' ', NULL, &dev_cbs[i].val);
			dev_cbs[i].name = devices[i];
			dev_cbs[i].comp = cb;

			newtGridSetField(devGrid, curcol, currow, NEWT_GRID_COMPONENT,
					      cb, 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
			curcol++;
			if (curcol >= cols) {
				currow++;
				curcol = 0;
			}
		}
		newtGridSetField (bigGrid, 1, currentRow, NEWT_GRID_SUBGRID, devGrid, 
			  1, 0, 0, 1, NEWT_ANCHOR_LEFT, 0);
		currentRow++;
	}
	 
	
	newtGridSetField (bigGrid, 0, currentRow, NEWT_GRID_COMPONENT,
			  newtLabel(-1, -1, N_("Allow incoming:")), 0, 0, 0, 0,
			  NEWT_ANCHOR_LEFT, 0);
	    
	portGrid = newtCreateGrid(3,2);

	
	for (i = 0; ports && ports[i].port; i++) {
		if (!strcmp(ports[i].proto,"tcp")) {
			switch (ports[i].port) {
			 case 22:
				ssh = 1;
				break;
			 case 23:
				telnet = 1;
				break;
			 case 80:
				http = 1;
				break;
			 case 25:
				smtp = 1;
				break;
			 case 21:
				ftp = 1;
				break;
			 default:
				porttext = add_portstr(porttext, &ports[i]);
				break;
				
			}
		} else {
			porttext = add_portstr(porttext, &ports[i]);
		}
	}
	    
	newtGridSetField(portGrid, 0, 0, NEWT_GRID_COMPONENT, 
			 newtCheckbox(-1, -1, N_("DHCP"), dhcp ? '*' : ' ', NULL, &dhcp_state),
			 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(portGrid, 1, 0, NEWT_GRID_COMPONENT, 
			 newtCheckbox(-1, -1, N_("SSH"), ssh ? '*' : ' ', NULL, &ssh_state),
			 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(portGrid, 2, 0, NEWT_GRID_COMPONENT, 
			 newtCheckbox(-1, -1, N_("Telnet"), telnet ? '*': ' ', NULL, &telnet_state),
			 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(portGrid, 0, 1, NEWT_GRID_COMPONENT, 
			 newtCheckbox(-1, -1, N_("WWW (HTTP)"), http ? '*' : ' ', NULL, &http_state),
			 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(portGrid, 1, 1, NEWT_GRID_COMPONENT,
			 newtCheckbox(-1, -1, N_("Mail (SMTP)"), smtp ? '*' : ' ', NULL, &smtp_state),
			 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(portGrid, 2, 1, NEWT_GRID_COMPONENT,
			 newtCheckbox(-1, -1, N_("FTP"), ftp ? '*' : ' ', NULL, &ftp_state),
			 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	
	oGrid = newtCreateGrid(2,1);
	newtGridSetField (oGrid, 0, 0, NEWT_GRID_COMPONENT,
			  newtLabel(-1, -1, N_("Other ports")), 
			  0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	other = newtEntry (-1, -1, "", 25, &portlist, NEWT_ENTRY_SCROLL);
	if (porttext)
	  newtEntrySet(other, porttext, 0);
	newtGridSetField(oGrid, 1, 0, NEWT_GRID_COMPONENT, other,
			 0, 0, 1, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(bigGrid, 1, currentRow, NEWT_GRID_SUBGRID, portGrid,
			 1, 0, 0, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(bigGrid, 0, currentRow + 1, NEWT_GRID_COMPONENT,
			 newtLabel(-1, -1, ""), 0, 0, 0, 1, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(bigGrid, 1, currentRow + 1, NEWT_GRID_SUBGRID,
			 oGrid, 1, 0, 0, 1, NEWT_ANCHOR_LEFT, 0);
	
	newtGridSetField(toplevel, 0, 0, NEWT_GRID_COMPONENT, textbox,
			 0, 0, 0, 0, NEWT_ANCHOR_LEFT, 0);
	newtGridSetField(toplevel, 0, 1, NEWT_GRID_SUBGRID, smallGrid,
			 0, 1, 0, 0, 0, 0);
	newtGridSetField(toplevel, 0, 2, NEWT_GRID_SUBGRID, bb,
			 0, 0, 0, 0, 0, NEWT_GRID_FLAG_GROWX);
		
	
	newtSetHelpCallback(helpCallback);
	
	newtInit();
        newtCls();
	newtPushHelpLine(N_(" <Tab>/<Alt-Tab> between elements   |   <Space> selects  |   <F12> next screen"));
	
	snprintf(roottext, 80,
		 N_("lokkit %s               "
		   "      (C) 2001 Red Hat, Inc."), VERSION);
	newtDrawRootText(0, 0, roottext);
	
	
	while (1) {
		newtComponent f;
		
		f = newtForm(NULL, "help", 0);
		newtGridWrappedWindow(toplevel, N_("Firewall Configuration"));
		
		newtGridAddComponentsToForm(toplevel, f, 1);
		
		answer = newtRunForm(f);

		if (answer == b_cancel) {
			newtPopWindow();
			newtFormDestroy(f);
			newtFinished();
			exit(0);
		}
		
		if (answer == b_custom) {
			newtComponent rb, popb_ok, pop_textbox, pop_f;
			newtGrid popbb;
			
			rb = newtRadioGetCurrent(paranoid_rb);
			if (rb == disabled_rb) {
				newtWinMessage(N_("Invalid Choice"),N_("Ok"),
					       N_("You cannot customize a disabled firewall."));
			} else {
				popbb = newtButtonBar (N_("OK"), &popb_ok, NULL);
	
				poplevel = newtCreateGrid(1, 3);
				
				pop_textbox = newtTextboxReflowed(-1, -1,
								  N_("You can customize your firewall in two ways. "
								    "First, you can select to allow all traffic from "
								    "certain network interfaces. Second, you can allow "
								    "certain protocols explicitly through the firewall. "
								    "Specify additional ports in the form 'service:protocol', "
								    "such as 'imap:tcp'. "), 
								  65, 5, 10, 0);
	

				newtGridSetField (poplevel, 0, 0, NEWT_GRID_COMPONENT,
						  pop_textbox, 0, 0, 0, 1, NEWT_ANCHOR_LEFT, 0);
				newtGridSetField (poplevel, 0, 1, NEWT_GRID_SUBGRID,
						  bigGrid, 0, 0, 0, 0, NEWT_ANCHOR_LEFT, 0);
				newtGridSetField (poplevel, 0, 2, NEWT_GRID_SUBGRID,
						  popbb, 0, 0, 0, 0, NEWT_ANCHOR_LEFT, NEWT_GRID_FLAG_GROWX);
	
				
				pop_f = newtForm(NULL, "help_custom", 0);
				
				newtGridWrappedWindow(poplevel,N_("Firewall Configuration - Customize"));
				newtGridAddComponentsToForm(poplevel, pop_f, 1);
				
				newtRunForm(pop_f);
				newtPopWindow();
			}
		}
		
		if (answer == b_ok) {
			break;
		}
		newtPopWindow();
	}

        newtPopWindow();
	newtFinished();
	
	if (devices) {
		for (i = 0; i < numdevices ; i++) {
			if (dev_cbs[i].val == '*') {
				add_dev(dev_cbs[i].name);
			}
		}
	}
	
	if (portlist) {
		char *tmp = portlist;
		while (*portlist) {
			while (*tmp && *tmp != ' ' && *tmp != ',') tmp++;
			if (*tmp) {
				*tmp = '\0';
				tmp++;
			}
			while (*tmp && (*tmp == ' ' || *tmp == ',')) tmp++;
			add_port(portlist);
			portlist = tmp;
		}
	}
	dhcp = dhcp_state == '*' ? 1 : 0;
	if (ssh_state == '*') {
		add_port(strdup("ssh:tcp"));
	}
	if (telnet_state == '*') {
		add_port(strdup("telnet:tcp"));
	}
	if (smtp_state == '*') {
		add_port(strdup("smtp:tcp"));
	}
	if (http_state == '*') {
		add_port(strdup("http:tcp"));
	}
	if (ftp_state == '*') {
		add_port(strdup("ftp:tcp"));
	}
	answer = newtRadioGetCurrent(paranoid_rb);
	if (answer == paranoid_rb) {
		disabled = medium = 0;
		high = 1;
	} else if (answer == simple_rb) {
		disabled = high = 0;
		medium = 1;
	} else {
		high = medium = 0;
		disabled = 1;
	}
}

int main(int argc, char **argv) {
	int type = 0;
	int ks = 0, nostart = 0;
	char *portspec, *devspec;
	poptContext context;
	int rc;
	struct poptOption options[] = {
		POPT_AUTOHELP
		{ "fwtype", 'f', POPT_ARG_NONE, &type, 0,
			  N_("Write Red Hat style /etc/sysconfig/ipchains rules, instead of a shell script"), 
			NULL
		},
		{ "quiet", 'q', POPT_ARG_NONE, &ks, 0,
			  N_("Run noninteractively; process only command-line arguments"),
			NULL 		},
	        { "nostart", 'n', POPT_ARG_NONE, &nostart, 0,
			  N_("Configure firewall but do not activate it"),
			NULL
		},
		{ "high",  0, POPT_ARG_NONE, &high, 0,
			  N_("Enable 'high' security level (default)"),
			NULL
		},
		{ "medium", 0, POPT_ARG_NONE, &medium, 0,
			  N_("Enable 'medium' security level"),
			NULL
		},
		{ "disabled", 0, POPT_ARG_NONE, &disabled, 0,
			  N_("Disable firewall"),
			NULL
		},
		{ "dhcp",  0, POPT_ARG_NONE, &dhcp, 0,
			  N_("Allow DHCP through the firewall"),
			NULL
		},
		{ "port",  'p', POPT_ARG_STRING, &portspec, 1,
			  N_("Allow specific ports through the firewall"),
			N_("port:protocol (e.g, ssh:tcp)")
		},
		{ "trust", 't', POPT_ARG_STRING, &devspec, 2,
			  N_("Allow all traffic on the specified device"),
			N_("device to trust")
		},
		{ 0, 0, 0, 0, 0, 0 }
	};
	
	if (getuid()) {
		fprintf(stderr,
			N_("\nERROR - You must be root to run lokkit.\n"));
		exit(1);
	}
	
	context = poptGetContext("lokkit", argc, argv, options, 0);
	while ((rc = poptGetNextOpt(context)) >= 0) {
		if (rc == 1) {
			add_port(portspec);
		} else if (rc == 2) {
			add_dev(devspec);
		}
	}
	if (rc < -1) {
		fprintf(stderr, "%s: %s\n",
 			poptBadOption(context, POPT_BADOPTION_NOALIAS),
			poptStrerror(rc));
		exit(1);
	}
	if ( ((high | medium) & disabled) || (high & medium) ) {
		fprintf(stderr, N_("ERROR - only one of 'high', 'medium', and 'disabled' may be specified.\n"));
		exit(1);
	}
	if (!(high || medium || disabled))
	  high = 1;
	if (type)
	  fwscript = strdup("/etc/sysconfig/ipchains");
	if (ks) {
		if (high)
		  write_firewall(0, devs, ports, dhcp, type, !nostart);
		else if (medium)
		  write_firewall(1, devs, ports, dhcp, type, !nostart);
		else if (disabled)
		  unlink(fwscript);
	} else {
		runInterface();
		if (high)
		  write_firewall(0, devs, ports, dhcp, type, !nostart);
		else if (medium)
		  write_firewall(1, devs, ports, dhcp, type, !nostart);
	}
	if (disabled) {
		system("/sbin/ipchains -F input");
		unlink(fwscript);
	}
	exit(0);
}
