#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h> 
#include <errno.h>
#include <sys/time.h>

#include <gnome.h>

#include "lokkit.h"

static char buffer[1025];
static int bufptr;
static int tag;

/*
 *	Handlers for the socket from the gnome link. We are called whenever
 *	data is ready. When we get a line we call line_received, when we get
 *	EOF we call line_eof after passing remaining data to line_received
 *	and when we error we call line_error.
 */
 
void host_read_event(gpointer data, gint fd, GdkInputCondition cond)
{
	int l=read(fd, buffer+bufptr, 1024-bufptr);
	char *bp;

	if(l==0)
	{
		gdk_input_remove(tag);
		buffer[bufptr]=0;
		line_received(buffer);
		line_eof();
		close(fd);
		return;
	}
	if(l<0)
	{
		if(errno==EWOULDBLOCK)
			return;
		gdk_input_remove(tag);
		line_error();
		close(fd);
		return;
	}
	
	bufptr+=l;
	buffer[bufptr]=0;
	
	while((bp=strchr(buffer, '\n'))!=NULL)
	{
		if(bp>buffer && bp[-1]=='\r')
			bp[-1]=0;
		*bp++=0;
		line_received(buffer);
		memmove(buffer,bp, 1024-bufptr);
		bufptr = 0;
	}
}
	

/*
 *	Attempt a connection
 */	
 
static int host_connect_attempt(struct in_addr ia, int port)
{
	int s=socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	struct sockaddr_in sin;
	
	fd_set wfd;
	struct timeval tv;
	
	if(s==-1)
	{
		net_error(_("Unable to allocate a socket"));
		return -1;
	}
	
	if(fcntl(s, F_SETFL, FNDELAY)==-1)
	{
		net_error(_("Unable to set connection nonblocking"));
		close(s);
		return -1;
	}

	sin.sin_family = AF_INET;	
	sin.sin_addr   = ia;
	sin.sin_port   = htons(port);
	
	if(connect(s, (struct sockaddr *)&sin, sizeof(sin))==-1 && errno != EINPROGRESS)
	{
		close(s);
		return -1;
	}	
	
	tv.tv_sec = 60;		/* We use 20 second timeouts for now */
	tv.tv_usec = 0;
	
	FD_ZERO(&wfd);
	FD_SET(s, &wfd);
	
	switch(select(s+1, NULL, &wfd, NULL, &tv))
	{
		case 0:
			/* Time out */
			net_error(_("Connection failed!"));
			close(s);
			return -1;
		case -1:
			/* Ermm.. ?? */
			net_error(_("Select failed!"));
			close(s);
			return -1;
	}
	
	return s;
}

/*
 *	Connect to a given host, port pair using the address list given
 *	back one by one until we get a success.
 */
 
int host_connect(const char *host, int port)
{
	struct hostent *h;
	int i;
	int s;
	
	h=gethostbyname(host);
	if(h==NULL)
	{
		net_error(_("Unable to resolve '%s'."), host);
		return -1;
	}
	
	
	for(i=0; h->h_addr_list[i]; i++)
	{
		struct in_addr ia;
		memcpy(&ia, h->h_addr_list[i],4);
		s=host_connect_attempt(ia, port);
		if(s != -1)
		{
			tag = gdk_input_add(s, GDK_INPUT_READ, 
				host_read_event, NULL);
			return s;
		}
	}
	net_error(_("Unable to connect to '%s'."), host);
	return -1;
}
