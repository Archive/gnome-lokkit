#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <gnome.h>
#include "lokkit.h"

int write_firewall(int policy, char **devs, struct port *ports, int dhcp,
		   int type, int activate)
{

	int i;
	FILE *fw=fopen(fwscript, "w");
	int num;
	struct in_addr *p;
	char buffer[128];
	
	if(fw==NULL)
		return -1;
	
	fprintf(fw, "#!/bin/sh\n");
	fprintf(fw, "PATH=/sbin:$PATH\n");
	
	fprintf(fw, "ipfwadm -I -f\n");	
	
	for(i=0;ports && ports[i].port;i++)
	  fprintf(fw, "ipfwadm -I -i accept -S 0/0 -D 0/0 %d -P %s %s\n",
		  ports[i].port, ports[i].proto, strcmp(ports[i].proto,"tcp") ? " " : "-y" );
	
	fprintf(fw, "ipfwadm -I -i accept -S 0/0 -D 0/0 -W lo\n");

	fprintf(fw, "ipfwadm -I -i accept -S 0/0 67:68 -D 0/0 67:68 -W eth0\n");
	fprintf(fw, "ipfwadm -I -i accept -S 0/0 67:68 -D 0/0 67:68 -W eth1\n");
	
	for (i = 0; devs && devs[i]; i++)
	    fprintf(fw, "ipfwadm -I -i accept -S 0/0 -D 0/0 -W %s\n",devs[i]);

	if(policy==0)
	{
		p=parse_resolv(&num);
		for(i=0;i<num;i++)
		{
			fprintf(fw, "ipfwadm -I -i accept -S %s 53 -D 0/0 -P udp\n",
				inet_ntoa(p[i]));
		}
		fprintf(fw, "ipfwadm -I -i deny -S 0/0 -D 0/0 -P tcp -y\n");
		fprintf(fw, "ipfwadm -I -i deny -S 0/0 -D 0/0 -P udp\n");
	}
	else
	{
		fprintf(fw, "ipfwadm -I -i deny -S 0/0 -D 0/0 0:1023 2049 -P tcp -y\n");
		fprintf(fw, "ipfwadm -I -i deny -S 0/0 -D 0/0 0:1023 2049 -P udp\n");
		fprintf(fw, "ipfwadm -I -i deny -S 0/0 -D 0/0 6000:6009 -P tcp -y\n");
	}
	if (activate) {
		sprintf(buffer, "/bin/sh %s", fwscript);
		system(buffer);
	}
	fclose(fw);
	return 0;
}


void flush_firewall(void)
{
	unlink(fwscript);
	system("/sbin/ipfwadm -I -f");
}

