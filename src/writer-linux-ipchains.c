#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "lokkit.h"

#define HEADER type ? "" : "ipchains "

int write_firewall(int policy, char **devs, struct port *ports, int dhcp,
		   int type, int activate)
{

	int i;
	FILE *fw = NULL;
	int num;
	struct in_addr *p;
	char buffer[128];
	
	fw = fopen(fwscript, "w");
	
	if(fw==NULL)
		return -1;
	
	if (type == 1) {
		fprintf(fw, "# Firewall configuration written by lokkit\n");
		fprintf(fw, "# Manual customization of this file is not recommended.\n");
		fprintf(fw, "# Note: ifup-post will punch the current nameservers through the\n");
		fprintf(fw, "#       firewall; such entries will *not* be listed here.\n"); 
		fprintf(fw, ":input ACCEPT\n");
		fprintf(fw, ":forward ACCEPT\n");
		fprintf(fw, ":output ACCEPT\n");
	} else {
		fprintf(fw, "#!/bin/sh\n");
		fprintf(fw, "PATH=/sbin:$PATH\n");
		fprintf(fw, "ipchains -F input\n");	
	}
	
	for(i=0;ports && ports[i].port;i++)
	  fprintf(fw, "%s-A input -s 0/0 -d 0/0 %d -p %s %s -j ACCEPT\n",
		  HEADER, ports[i].port, ports[i].proto,
		  strcmp(ports[i].proto,"tcp") ? " " : "-y");
	  
	if(dhcp)
	{
		fprintf(fw,"%s-A input -s 0/0 67:68 -d 0/0 67:68 -p udp -i eth0 -j ACCEPT\n",
			HEADER);
		fprintf(fw,"%s-A input -s 0/0 67:68 -d 0/0 67:68 -p udp -i eth1 -j ACCEPT\n",
			HEADER);
	}
	
	fprintf(fw, "%s-A input -s 0/0 -d 0/0 -i lo -j ACCEPT\n", HEADER);
	
	for (i=0; devs && devs[i]; i++)
	  fprintf(fw, "%s-A input -s 0/0 -d 0/0 -i %s -j ACCEPT\n",HEADER, devs[i]);

	if(policy==0)
	{
		p=parse_resolv(&num);
		for(i=0;i<num;i++)
		{
			fprintf(fw, "%s-A input -s %s 53 -d 0/0 -p udp -j ACCEPT\n",
				HEADER, inet_ntoa(p[i]));
		}
		fprintf(fw, "%s-A input -s 0/0 -d 0/0 -p tcp -y -j REJECT\n", HEADER);
		fprintf(fw, "%s-A input -s 0/0 -d 0/0 -p udp -j REJECT\n", HEADER);
	}
	else
	{
		fprintf(fw, "%s-A input -p tcp -s 0/0 -d 0/0 0:1023 -y -j REJECT\n", HEADER);
		fprintf(fw, "%s-A input -p tcp -s 0/0 -d 0/0 2049 -y -j REJECT\n", HEADER);
		fprintf(fw, "%s-A input -p udp -s 0/0 -d 0/0 0:1023 -j REJECT\n", HEADER);
		fprintf(fw, "%s-A input -p udp -s 0/0 -d 0/0 2049 -j REJECT\n", HEADER);
		fprintf(fw, "%s-A input -p tcp -s 0/0 -d 0/0 6000:6009 -y -j REJECT\n", HEADER);
		fprintf(fw, "%s-A input -p tcp -s 0/0 -d 0/0 7100 -y -j REJECT\n", HEADER);
	}
	fclose(fw);
	chmod(fwscript,0600);
	
	if (activate) {
		/* The 2.4 kernel won't load this on demand, apparently. */
		sprintf(buffer,"/sbin/modprobe ipchains >/dev/null 2>&1");
		system(buffer);
	
		if (type == 1) {
			sprintf(buffer, "/sbin/service ipchains start >/dev/null 2>&1");
			system(buffer);
		} else {
			sprintf(buffer, "/bin/sh %s", fwscript);
			system(buffer);
		}
	}
	return 0;
}


void flush_firewall(void)
{
	unlink(fwscript);
	system("/sbin/ipchains -F input");
}

