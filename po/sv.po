# Swedish messages for gnome-lokkit.
# Copyright (C) 2000, 2001, 2002 Free Software Foundation, Inc.
# Anders Carlsson <andersca@gnu.org>, 2000.
# Christian Rose <menthos@menthos.com>, 2000, 2001, 2002.
#
# $Id$
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-lokkit\n"
"POT-Creation-Date: 2002-01-09 01:26+0100\n"
"PO-Revision-Date: 2002-01-09 01:26+0100\n"
"Last-Translator: Christian Rose <menthos@menthos.com>\n"
"Language-Team: Swedish <sv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/gnome.c:33 src/newt.c:601
msgid ""
"Write Red Hat style /etc/sysconfig/ipchains rules, instead of a shell script"
msgstr ""
"Skriv /etc/sysconfig/ipchains-regler p� Red Hat-vis, ist�llet f�r ett "
"skalskript"

#: src/gnome.c:38
msgid ""
"Lokkit is a tool to provide firewalling for the average\n"
"Linux end user. Instead of having to configure firewall\n"
"rules the Lokkit program asks a small number of simple\n"
"questions and writes a firewall rule set for you.\n"
"\n"
"Lokkit is not designed to configure arbitary firewalls.\n"
"To make it simple to understand it is solely designed to\n"
"handle typical dialup user and cable modem setups.\n"
"\n"
"It is not the equal of an expert firewall designer."
msgstr ""
"Lokkit �r ett verktyg f�r att tillhandah�lla brandv�ggar f�r den\n"
"genomsnittlige Linux-slutanv�ndaren. I st�llet f�r att beh�va\n"
"konfigurera brandv�ggsregler st�ller Lokkit ett litet antal enkla\n"
"fr�gor och skapar sedan en samling brandv�ggsregler �t dig.\n"
"\n"
"Lokkit �r inte konstruerat f�r att konfigurera vilka brandv�ggar\n"
"som helst. F�r att det ska vara enkelt att f�rst� �r det bara\n"
"gjort f�r att hantera typiska modemuppkopplings- och\n"
"kabelmodemskonfigurationer.\n"
"\n"
"Lokkit �r ingen ers�ttning f�r en erfaren brandv�ggskonstrukt�r."

#: src/gnome.c:48
msgid ""
"Lokkit is about to activate firewalling on this system.\n"
"It is strongly recommended you only activate the \n"
"firewalling facilities while you have direct access \n"
"to the machine, as you may not be able to get access \n"
"to deactivate them remotely.\n"
msgstr ""
"Lokkit kommer nu att aktivera brandv�ggen p� detta system.\n"
"Det rekommenderas att du bara aktiverar brandv�ggen n�r du\n"
"har direkt tillg�ng till maskinen, eftersom du kanske inte\n"
"kan n� maskinen utifr�n f�r att kunna st�nga av brandv�ggen.\n"

#: src/gnome.c:105 src/gnome.c:499
msgid "Configure Firewalling"
msgstr "Konfigurera brandv�gg"

#: src/gnome.c:135
msgid "Activate The Firewall"
msgstr "Aktivera brandv�ggen"

#. Error dialog
#: src/gnome.c:434
msgid "Unable to write firewall rules"
msgstr "Kan inte skriva brandv�ggsregler"

#: src/gnome.c:448
msgid "Mail services are enabled. Checking for relaying"
msgstr "E-posttj�nster �r p�slagna. Kontrollerar vidarebefordran (relaying)"

#: src/gnome.c:476
msgid "You need to be the superuser to use this application."
msgstr "Du m�ste vara superanv�ndaren f�r att kunna anv�nda detta program."

#: src/gnome.c:485
msgid ""
"You are about to override your old firewall configuration.\n"
" Are you sure you want to continue?\n"
msgstr ""
"Du h�ller p� att ers�tta din gamla brandv�ggskonfiguration.\n"
" �r du s�ker p� att du vill forts�tta?\n"

#: src/gnome.c:507
msgid "Basic Configuration"
msgstr "Grundinst�llningar"

#: src/gnome.c:508
msgid ""
"Lokkit can set up either a low or high security configuration. A high "
"security configuration blocks all incoming accesses but requires you use "
"specific ftp configurations and blocks irc dcc. The lower security option "
"blocks system services but is not so intrusive. "
msgstr ""
"Lokkit kan s�tta upp en konfiguration med antingen l�g eller h�g s�kerhet. "
"Med h�g s�kerhet blockeras alla inkommande �tkomster men d� kr�vs det att "
"s�rskilda ftp-konfigureringar anv�nds, och det blockerar dcc i irc. Med l�g "
"s�kerhet blockeras systemtj�nster men inte lika restriktivt."

#: src/gnome.c:513
msgid "High Security - more intrusive"
msgstr "H�g s�kerhet - mer restriktiv"

#: src/gnome.c:514
msgid "Low Security  - less intrusive"
msgstr "L�g s�kerhet - mindre restriktiv"

#: src/gnome.c:515
msgid "Disable Firewall - no protection"
msgstr "Inaktivera brandv�gg - inget skydd"

#: src/gnome.c:523 src/gnome.c:535
msgid "Local Hosts"
msgstr "Lokala v�rdar"

#: src/gnome.c:524
msgid ""
"You seem to have an ethernet (eth0) attached to your machine. Shall I trust "
"the hosts that are connected via this card? If this connection is your link "
"to the internet (eg cable modem) you should say no."
msgstr ""
"Det verkar som om du har ett ethernet-kort (eth0) anslutet till din dator. "
"�r de datorer som kommer �t din dator via det kortet p�litliga? Om denna "
"anslutning �r din l�nk till Internet (exempelvis kabelmodem) b�r du svara "
"nej."

#: src/gnome.c:528 src/gnome.c:540 src/gnome.c:550 src/gnome.c:560
#: src/gnome.c:573 src/gnome.c:584 src/gnome.c:596 src/gnome.c:608
msgid "   Yes   "
msgstr "   Ja    "

#: src/gnome.c:529 src/gnome.c:541 src/gnome.c:551 src/gnome.c:561
#: src/gnome.c:574 src/gnome.c:585 src/gnome.c:597 src/gnome.c:609
msgid "   No    "
msgstr "   Nej   "

#: src/gnome.c:536
msgid ""
"You seem to have a second ethernet (eth1) attached to your machine. Shall I "
"trust the hosts that are connected via this card? If this connection is your "
"link to the internet (eg cable modem) you should say no."
msgstr ""
"Det verkar som om du har ett andra ethernet-kort (eth1) anslutet till din "
"dator. �r de datorer som kommer �t din dator via det kortet p�litliga? Om "
"denna anslutning �r din l�nk till Internet (exemepelvis kabelmodem) b�r du "
"svara nej."

#: src/gnome.c:546 src/newt.c:421
msgid "DHCP"
msgstr "DHCP"

#: src/gnome.c:547
msgid ""
"Are you using DHCP on any of your interfaces? If you are then say yes and "
"the firewall rules will allow DHCP clients to negotiate addresses with your "
"provider."
msgstr ""
"Anv�nder du DHCP p� n�got av dina n�tverksgr�nssnitt? Om du g�r det svarar "
"du ja, s� kommer brandv�ggsreglerna att till�ta DHCP-klienter att komma "
"�verens om adresser med din leverant�r."

#: src/gnome.c:555
msgid "Services"
msgstr "Tj�nster"

#: src/gnome.c:556
msgid ""
"You can enable access to some of the incoming services on your machine. "
"Before doing so you should check you are up to date with any errata from "
"your Linux vendor.\n"
"Do you wish to select services to enable.\n"
msgstr ""
"Du kan aktivera tillg�ng till n�gra av de inkommande tj�nsterna p� din "
"dator. Innan du g�r det b�r du kontrollera att du har uppdaterat programmen "
"p� din dator med de senaste fixarna till din Linuxdistribution.\n"
"Vill du v�lja tj�nster som ska aktiveras?\n"

#: src/gnome.c:568
msgid "Web Server"
msgstr "Webbserver"

#: src/gnome.c:569
msgid ""
"This allows people to access and view the web server on your computer. You "
"do not need to enable this to use the web server directly from the machine "
"itself for things like developing pages."
msgstr ""
"Detta l�ter andra komma �t och se webbservern p� din dator. Du beh�ver inte "
"aktivera den h�r tj�nsten f�r att kunna anv�nda webbservern direkt fr�n din "
"dator f�r att exempelvis utveckla webbsidor."

#: src/gnome.c:580
msgid "Incoming Mail"
msgstr "Inkommande e-post"

#: src/gnome.c:581
msgid ""
"Allow incoming SMTP mail delivery. You do not need to enable this if you "
"collect your mail from your ISP's server by POP3 or IMAP, or if you use a "
"tool such as fetchmail."
msgstr ""
"Till�t leverans av inkommande e-post �ver SMTP. Du beh�ver inte aktivera "
"denn h�r tj�nsten om du h�mtar din post med POP3 eller IMAP fr�n din "
"Internetleverant�rs server, eller om du anv�nder ett verktyg som fetchmail."

#: src/gnome.c:591
msgid "Secure Shell"
msgstr "Secure Shell"

#: src/gnome.c:592
msgid ""
"Allow incoming secure shell access. This is an encrypted way to talk to your "
"computer over the internet. You may also wish to read the ssh documentation "
"and set up a list of hosts permitted to connect."
msgstr ""
"Till�t inkommande secure shell-anslutningar. Secure shell �r ett s�tt att "
"ansluta krypterat till din dator �ver Internet. Du b�r l�sa ssh-"
"dokumentationen och s�tta upp en lista �ver datorer som till�ts ansluta."

#: src/gnome.c:603 src/newt.c:427
msgid "Telnet"
msgstr "Telnet"

#: src/gnome.c:604
msgid ""
"Allow incoming telnet access. This allows access to the command line shell "
"of the machine from outside, if you have a username and password. It is not "
"encrypted and can therefore be spied upon. If possible avoid enabling this."
msgstr ""
"Till�t inkommande telnet-anslutningar. Telnet ger dig tillg�ng till din "
"dators kommandorad utifr�n, om du har ett anv�ndarnamn och l�senord. "
"Anslutningen �r inte krypterad och kan d�rf�r avlyssnas. F�rs�k att undvika "
"att aktivera telnet om det �r m�jligt."

#: src/libinternet.c:82
msgid "Unable to allocate a socket"
msgstr "Kan inte allokera ett uttag (socket)"

#: src/libinternet.c:88
msgid "Unable to set connection nonblocking"
msgstr "Kan inte g�ra anslutning icke-blockerande"

#. Time out
#: src/libinternet.c:113
msgid "Connection failed!"
msgstr "Anslutning misslyckades!"

#. Ermm.. ??
#: src/libinternet.c:118
msgid "Select failed!"
msgstr "Select misslyckades!"

#: src/libinternet.c:140
#, c-format
msgid "Unable to resolve '%s'."
msgstr "Kunde inte sl� upp \"%s\"."

#: src/libinternet.c:157
#, c-format
msgid "Unable to connect to '%s'."
msgstr "Kunde inte ansluta till \"%s\"."

#: src/scanomatic.c:34
msgid "Mail relay testing"
msgstr "Testande av postvidarebefordran (relay)"

#: src/scanomatic.c:37
msgid "Beginning mail test."
msgstr "P�b�rjar posttest."

#: src/scanomatic.c:48
#, c-format
msgid "Mail relay check %p%% complete"
msgstr "E-posttestet %p%% f�rdigt"

#: src/scanomatic.c:145 src/scanomatic.c:159
msgid "Relay test result."
msgstr "Testresultat f�r vidarebefordran."

#: src/scanomatic.c:170
msgid "Beginning mail tests"
msgstr "P�b�rjar posttest"

#: src/scanomatic.c:201
msgid "EOF - passed"
msgstr "Filslut - gick igenom"

#: src/scanomatic.c:204
msgid "Failed test sequence is: \n"
msgstr "Testsekvensen som misslyckades �r: \n"

#: src/scanomatic.c:206
msgid "missing!\n"
msgstr "saknas!\n"

#: src/scanomatic.c:210
msgid "http://maps.vix.com/tsi/"
msgstr "http://maps.vix.com/tsi/"

#: src/scanomatic.c:224
msgid "Unexpected end of file"
msgstr "Ov�ntat filslut"

# Kriminellt galen l�ngd p� meddelandet rapporterat i
# http://bugzilla.gnome.org/show_bug.cgi?id=61490
#
# Tyv�rr verkar inte f�rfattaren f�rst� att det �r ett problem
#
#: src/newt.c:64
msgid ""
"\n"
"Firewall Configuration\n"
"\n"
"   Red Hat Linux also offers you firewall protection\n"
"   for enhanced system security. A firewall sits\n"
"   between your computer and the network, and\n"
"   determines which resources on your computer remote\n"
"   users on the network are able to access. A\n"
"   properly configured firewall can greatly increase\n"
"   the out-of-the-box security of your system.\n"
"\n"
"   Choose the appropriate security level for your\n"
"   system.\n"
"\n"
"   High Security -- By choosing High Security, your\n"
"   system will not accept connections that are not\n"
"   explicitly defined by you. By default, only the\n"
"   following connections are allowed:\n"
"\n"
"     * DNS replies\n"
"     * DHCP -- so any network interfaces that use\n"
"       DHCP can be properly configured.\n"
"\n"
"   Using this High Security will not allow the\n"
"   following:\n"
"\n"
"     * Active mode FTP (Passive mode FTP, used by\n"
"       default in most clients, should work fine.)\n"
"     * IRC DCC file transfers\n"
"     * RealAudio(tm)\n"
"     * Remote X Window System clients\n"
"\n"
"   If you are connecting your system to the Internet,\n"
"   but do not plan to run a server, this is the\n"
"   safest choice. If additional services are needed,\n"
"   you can choose Customize to allow specific\n"
"   services through the firewall.\n"
"\n"
"   Medium Security -- Choosing Medium Security will\n"
"   not allow your system to have access to certain\n"
"   resources. By default, access to the following\n"
"   resources are not allowed:\n"
"\n"
"     * ports lower than 1023 -- these are the\n"
"       standard reserved ports, used by most system\n"
"       services, such as FTP, SSH, telnet, and HTTP.\n"
"     * NFS server port (2049)\n"
"     * the local X Window System display for remote X\n"
"       clients\n"
"     * the X Font server port (This is disabled by\n"
"       default in the font server.)\n"
"\n"
"   If you want to allow resources such as\n"
"   RealAudio(tm), while still blocking access to\n"
"   normal system services, choose Medium Security.\n"
"   You can choose Customize to allow specific\n"
"   services through the firewall.\n"
"\n"
"   No Firewall -- No firewall allows complete access\n"
"   and does no security checking. It is recommended\n"
"   that this only be selected if you are running on a\n"
"   trusted network (not the Internet), or if you plan\n"
"   to do more detailed firewall configuration later.\n"
"\n"
"   Choose Customize to add trusted devices or to\n"
"   allow additional incoming interfaces.\n"
msgstr ""
"\n"
"Brandv�ggskonfiguration\n"
"\n"
"   Red Hat Linux erbjuder dig brandv�ggsskydd f�r\n"
"   f�rb�ttrad systems�kerhet. En brandv�gg befinner\n"
"   sig mellan din dator och n�tverket, och avg�r vilka\n"
"   resurser p� din dator som fj�rranv�ndare p� n�tverket\n"
"   kan komma �t. En korrekt konfigurerad brandv�gg kan\n"
"   dramatiskt f�rb�ttra den standardm�ssiga s�kerheten\n"
"   p� ditt system.\n"
"\n"
"   V�lj l�mplig s�kerhetsniv� f�r ditt system.\n"
"\n"
"   H�g s�kerhet -- Genom att v�lja h�g s�kerhet kommer\n"
"   ditt system inte att acceptera anslutningar som inte\n"
"   �r uttryckligen till�tna av dig. Som standard �r\n"
"   endast f�ljande anslutningar till�tna:\n"
"\n"
"     * DNS-svar\n"
"     * DHCP -- s� att alla n�tverksgr�nssnitt som\n"
"       anv�nder DHCP kan konfigureras korrekt.\n"
"\n"
"   Att anv�nda denna h�ga s�kerhet kommer inte att\n"
"   till�ta f�ljande:\n"
"\n"
"     * FTP i aktivt l�ge (FTP i passivt l�ge, som �r\n"
"       standardalternativet i de flesta klienter, b�r\n"
"       fungera).\n"
"     * DCC-fil�verf�ringar i IRC\n"
"     * RealAudio(tm)\n"
"     * X Window System-fj�rrklienter\n"
"\n"
"   Om du t�nker ansluta ditt system till Internet, men\n"
"   inte planerar att k�ra en server, �r detta det\n"
"   s�kraste alternativet. Om ytterligare tj�nster\n"
"   kr�vs kan du v�lja Anpassa f�r att till�ta\n"
"   specifika tj�nster genom brandv�ggen.\n"
"\n"
"   Mellans�kerhet -- Att v�lja mellans�kerhet kommer\n"
"   inte l�ta ditt system ha tillg�ng till vissa\n"
"   resurser. Som standard kommer f�ljande resurser\n"
"   inte att till�tas:\n"
"\n"
"     * portar l�gre �n 1023 -- dessa �r reserverade\n"
"       standardportar som anv�nds av de flesta\n"
"       systemtj�nster, som exempelvis FTP, SSH,\n"
"       telnet och HTTP.\n"
"     * NFS-serverporten (2049)\n"
"     * Den lokala X Window System-displayen f�r\n"
"       X-fj�rrklienter.\n"
"     * X-typsnittsserverporten (denna �r inaktiverad\n"
"       som standard i typsnittsservern).\n"
"\n"
"   Om du vill till�ta resurser s�som RealAudio(tm)\n"
"   samtidigt som du blockerar �tkomst till normala\n"
"   systemtj�nster b�r du v�lja mellans�kerhet.\n"
"   Du kan v�lja Anpassa f�r att l�ta specifika\n"
"   tj�nster passera genom brandv�ggen.\n"
"\n"
"   Ingen brandv�gg -- Ingen brandv�gg till�ter\n"
"   fullst�ndig �tkomst och utf�r ingen s�kerhets-\n"
"   kontroll. Det rekommenderas att du endast\n"
"   anv�nder detta om du anv�nder systemet p� ett\n"
"   p�litligt n�tverk (inte Internet), eller om du\n"
"   planerar att g�ra en mer detaljerad brandv�ggs-\n"
"   konfiguration senare.\n"
"\n"
"   V�lj Anpassa f�r att l�gga till p�litliga\n"
"   enheter eller f�r att till�ta ytterligare\n"
"   inkommande gr�nssnitt.\n"

# Kriminellt galen l�ngd p� meddelandet rapporterat i
# http://bugzilla.gnome.org/show_bug.cgi?id=61490
#
# Tyv�rr verkar f�rfattaren inte f�rst� att det �r ett problem
#
#: src/newt.c:130
msgid ""
"\n"
"Firewall Customization\n"
"\n"
"   Choose which trusted devices and incoming services\n"
"   should be allowed for your network security\n"
"   settings.\n"
"\n"
"   Trusted Devices -- Checking these for any of your\n"
"   devices allows all traffic coming from that device\n"
"   to be allowed. For example, if you are running a\n"
"   local network, but are connecting to the Internet\n"
"   via a PPP dialup, you could check that eth0 is\n"
"   trusted to allow any traffic coming from your\n"
"   local network.\n"
"\n"
"   It is not recommended to enable this for devices\n"
"   that are connected to public networks, such as the\n"
"   Internet.\n"
"\n"
"   Allow Incoming -- Enabling these options allow the\n"
"   specified services to pass through the firewall.\n"
"   Note, during a workstation-class installation, the\n"
"   majority of these services are not present on the\n"
"   system.\n"
"\n"
"     * DHCP -- This allows DHCP queries and replies,\n"
"       and allows any network interfaces that use\n"
"       DHCP to determine their IP address. DHCP is\n"
"       normally enabled.\n"
"     * SSH -- Secure Shell (SSH) is a protocol for\n"
"       logging into and executing commands on remote\n"
"       machines. It provides secure encrypted\n"
"       communications. If you plan on accessing your\n"
"       machine remotely via SSH over a firewalled\n"
"       interface, enable this option. You need the\n"
"       openssh-server package installed for this\n"
"       option to be useful.\n"
"     * Telnet -- Telnet is a protocol for logging\n"
"       into remote machines. It is unencrypted, and\n"
"       provides little security from network snooping\n"
"       attacks. Enabling telnet is not recommended.\n"
"       You need the telnet-server package installed\n"
"       for this option to be useful.\n"
"     * WWW (HTTP) -- HTTP is the protocol used by\n"
"       Apache to serve Web pages. If you plan on\n"
"       making your Web server publicly available,\n"
"       enable this option. This option is not\n"
"       required for viewing pages locally or\n"
"       developing Web pages. You need the Apache\n"
"       package installed for this option to be\n"
"       useful.\n"
"     * Mail (SMTP) -- This allows incoming SMTP mail\n"
"       delivery. If you need to allow remote hosts to\n"
"       connect directly to your machine to deliver\n"
"       mail, enable this option. You do not need to\n"
"       enable this if you collect your mail from your\n"
"       ISP's server by POP3 or IMAP, or if you use a\n"
"       tool such as fetchmail. Note that an\n"
"       improperly configured SMTP server can allow\n"
"       remote machines to use your server to send\n"
"       spam.\n"
"     * FTP -- FTP is a protocol used for remote file\n"
"       transfer. If you plan on making your FTP\n"
"       server publicly available, enable this option.\n"
"       You need the wu-ftpd (and possibly anonftp)\n"
"       packages installed for this option to be\n"
"       useful.\n"
"     * Other ports -- You can specify that other\n"
"       ports not listed here be allowed through the\n"
"       firewall. The format to use is\n"
"       'port:protocol'. For example, if you wanted to\n"
"       allow IMAP access through your firewall, you\n"
"       can specify 'imap:tcp'. You can also specify\n"
"       numeric ports explicitly; to allow UDP packets\n"
"       on port 1234 through, specify '1234:udp'. To\n"
"       specify multiple ports, separate them by\n"
"       commas.\n"
msgstr ""
"\n"
"Brandv�ggsanpassning\n"
"\n"
"   V�lj vilka p�litliga enheter och inkommande\n"
"   tj�nster som ska till�tas f�r dina n�tverks-\n"
"   s�kerhetsinst�llningar.\n"
"\n"
"   P�litliga enheter -- Att kryssa f�r detta f�r n�gon\n"
"   av dina enheter g�r att all trafik som kommer fr�n\n"
"   den enheten till�ts. Om du till exempel anv�nder ett\n"
"   lokalt n�tverk, men ansluter till Internet via en\n"
"   PPP-modemuppkoppling, kan du markera att eth0 �r\n"
"   anf�rtrodd att till�ta all trafik som kommer fr�n ditt\n"
"   lokala n�tverk.\n"
"\n"
"   Att anv�nda detta f�r enheter som �r anslutna till\n"
"   allm�nna n�tverk, som exempelvis Internet,\n"
"   rekommenderas inte.\n"
"\n"
"   Till�t inkommande -- Att anv�nda detta alternativ\n"
"   till�ter de angivna tj�nsterna att passera genom\n"
"   brandv�ggen.\n"
"   Observera att under en arbetsstationsinstallation\n"
"   finns de flesta av dessa tj�nster inte p� systemet.\n"
"\n"
"     * DHCP -- Detta till�ter DHCP-fr�gor och -svar,\n"
"       och till�ter alla n�tverksgr�nssnitt som\n"
"       anv�nder DHCP att best�mma deras IP-adress.\n"
"       DHCP �r normalt p�slaget.\n"
"     * SSH -- Secure Shell (SSH) �r ett protokoll f�r\n"
"       inloggning och exekvering av kommandon p�\n"
"       fj�rrmaskiner. Det tillhandah�ller s�ker,\n"
"       krypterad kommunikation. Om du planerar att\n"
"       ansluta till din maskin med SSH �ver n�tverket\n"
"       och �ver ett gr�nssnitt med brandv�ggsskydd b�r\n"
"       du anv�nda detta alternativ. Du beh�ver ha\n"
"       paketet openssh-server installerat f�r att\n"
"       detta alternativ ska vara anv�ndbart.\n"
"     * Telnet -- Telnet �r ett protokoll f�r inloggning\n"
"       p� fj�rrmaskiner. Det �r okrypterat och erbjuder\n"
"       v�ldigt lite s�kerhet mot n�tverksavlyssnings-\n"
"       attacker. Att anv�nda telnet rekommenderas inte.\n"
"       Du beh�ver ha paketet telnet-server installerat\n"
"       f�r att detta alternativ ska vara anv�ndbart.\n"
"     * WWW (HTTP) -- HTTP �r protokollet som anv�nds\n"
"       av Apache f�r att tillhandah�lla webbsidor.\n"
"       Om du planerar att g�ra din webbserver allm�nt\n"
"       tillg�nglig anv�nder du detta alternativ.\n"
"       Detta alternativ kr�vs inte f�r att l�sa\n"
"       lokala sidor eller utveckla webbsidor. Du\n"
"       beh�ver paketet Apache f�r att detta\n"
"       alternativ ska vara anv�ndbart.\n"
"     * E-post (SMTP) -- Detta till�ter leverans av\n"
"       inkommande e-post. Om du m�ste till�ta fj�rr-\n"
"       v�rdar att ansluta direkt till din maskin och\n"
"       leverera e-post anv�nder du detta alternativ.\n"
"       Du beh�ver inte anv�nda detta om du h�mtar din\n"
"       e-post fr�n din Internetleverant�rs server med\n"
"       hj�lp av POP3 eller IMAP, eller om du anv�nder\n"
"       ett verktyg som fetchmail. Observera att en\n"
"       felaktigt konfigurerad SMTP-server kan till�ta\n"
"       fj�rrmaskiner att anv�nda din server f�r att\n"
"       skicka skr�ppost (spam).\n"
"     * FTP -- FTP �r ett protokoll f�r fj�rr-\n"
"       fil�verf�ring. Om du planerar att g�ra din FTP-\n"
"       server allm�nt tillg�nglig anv�nder du detta\n"
"       alternativ. Du m�ste ha paketen wu-ftpd (och\n"
"       m�jligen anonftp) installerade f�r att detta\n"
"       alternativ ska vara anv�ndbart.\n"
"     * Andra portar -- Du kan ange att andra portar\n"
"       som inte anges h�r till�ts genom brandv�ggen.\n"
"       Formatet att anv�nda �r \"port:protokoll\".\n"
"       Om du till exempel vill till�ta IMAP-�tkomst\n"
"       genom din brandv�gg kan du ange \"imap:tcp\".\n"
"       Du kan ocks� ange numeriska portar. F�r att\n"
"       l�ta UDP-paket p� port 1234 passera anger du\n"
"       \"1234:udp\". F�r att ange flera portar anger\n"
"       du dem �tskilda med komman.\n"

#: src/newt.c:210 src/newt.c:479
msgid "Firewall Configuration"
msgstr "Brandv�ggskonfiguration"

#: src/newt.c:210 src/newt.c:212 src/newt.c:498
msgid "Ok"
msgstr "Ok"

#: src/newt.c:212
msgid "Firewall Customization"
msgstr "Brandv�ggsanpassning"

#: src/newt.c:306 src/newt.c:501
msgid "OK"
msgstr "OK"

#: src/newt.c:306
msgid "Customize"
msgstr "Anpassa"

#: src/newt.c:306
msgid "Cancel"
msgstr "Avbryt"

#: src/newt.c:309
msgid ""
"A firewall protects against unauthorized network intrusions. High security "
"blocks all incoming accesses. Medium blocks access to system services (such "
"as telnet or printing), but allows other connections. No firewall allows all "
"connections and is not recommended. "
msgstr ""
"En brandv�gg skyddar mot otill�tna n�tverksintr�ng. H�g s�kerhet blockerar "
"alla inkommande anslutningar. Mellan blockerar �tkomst till systemtj�nster "
"(exempelvis telnet eller utskrift), men till�ter andra anslutningar. Ingen "
"brandv�gg till�ter alla anslutningar och rekommenderas inte."

#: src/newt.c:323
msgid "Security Level:"
msgstr "S�kerhetsniv�:"

#: src/newt.c:325
msgid "High"
msgstr "H�g"

#: src/newt.c:328
msgid "Medium"
msgstr "Mellan"

#: src/newt.c:331
msgid "No firewall"
msgstr "Ingen brandv�gg"

#: src/newt.c:355
msgid "Trusted Devices:"
msgstr "P�litliga enheter:"

#: src/newt.c:386
msgid "Allow incoming:"
msgstr "Till�t inkommande:"

#: src/newt.c:424
msgid "SSH"
msgstr "SSH"

#: src/newt.c:430
msgid "WWW (HTTP)"
msgstr "WWW (HTTP)"

#: src/newt.c:433
msgid "Mail (SMTP)"
msgstr "E-post (SMTP)"

#: src/newt.c:436
msgid "FTP"
msgstr "FTP"

#: src/newt.c:441
msgid "Other ports"
msgstr "Andra portar"

#: src/newt.c:467
msgid ""
" <Tab>/<Alt-Tab> between elements   |   <Space> selects  |   <F12> next "
"screen"
msgstr ""
"  <Tab>/<Alt-Tab> mellan element  |  <Blanksteg> v�ljer  |  <F12> n�sta sk�rm"

#: src/newt.c:470
#, c-format
msgid "lokkit %s                     (C) 2001 Red Hat, Inc."
msgstr "lokkit %s                     � 2001 Red Hat, Inc."

#: src/newt.c:498
msgid "Invalid Choice"
msgstr "Ogiltigt val"

#: src/newt.c:499
msgid "You cannot customize a disabled firewall."
msgstr "Du kan inte anpassa en inaktiverad brandv�gg."

#: src/newt.c:506
msgid ""
"You can customize your firewall in two ways. First, you can select to allow "
"all traffic from certain network interfaces. Second, you can allow certain "
"protocols explicitly through the firewall. Specify additional ports in the "
"form 'service:protocol', such as 'imap:tcp'. "
msgstr ""
"Du kan anpassa din brandv�gg p� tv� s�tt. Till att b�rja med kan du v�lja "
"att till�ta all trafik fr�n vissa n�tverkskgr�nssnitt. Du kan �ven v�lja att "
"uttryckligen till�ta att vissa protokoll f�r passera brandv�ggen. Ange "
"ytterligare portar p� formen \"tj�nst:protokoll\", till exempel \"imap:tcp\"."

#: src/newt.c:525
msgid "Firewall Configuration - Customize"
msgstr "Brandv�ggskonfiguration - Anpassa"

#: src/newt.c:605
msgid "Run noninteractively; process only command-line arguments"
msgstr "K�r ickeinteraktivt; behandla endast kommandoradsargument"

#: src/newt.c:608
msgid "Configure firewall but do not activate it"
msgstr "Konfigurera brandv�gg men aktivera den inte"

#: src/newt.c:612
msgid "Enable 'high' security level (default)"
msgstr "Anv�nd \"h�g\" s�kerhetsniv� (standard)"

#: src/newt.c:616
msgid "Enable 'medium' security level"
msgstr "Anv�nd \"mellan\"-s�kerhetsniv�"

#: src/newt.c:620
msgid "Disable firewall"
msgstr "Inaktivera brandv�gg"

#: src/newt.c:624
msgid "Allow DHCP through the firewall"
msgstr "Till�t DHCP genom brandv�ggen"

#: src/newt.c:628
msgid "Allow specific ports through the firewall"
msgstr "Till�t specifika portar genom brandv�ggen"

#: src/newt.c:629
msgid "port:protocol (e.g, ssh:tcp)"
msgstr "port:protokoll (exempelvis ssh:tcp)"

#: src/newt.c:632
msgid "Allow all traffic on the specified device"
msgstr "Till�t all trafik p� angiven enhet"

#: src/newt.c:633
msgid "device to trust"
msgstr "p�litlig enhet"

#: src/newt.c:640
msgid ""
"\n"
"ERROR - You must be root to run lokkit.\n"
msgstr ""
"\n"
"FEL - Du m�ste vara root f�r att k�ra lokkit.\n"

#: src/newt.c:659
msgid ""
"ERROR - only one of 'high', 'medium', and 'disabled' may be specified.\n"
msgstr "FEL - endast en av \"high\", \"medium\" och \"disabled\" kan anges.\n"
