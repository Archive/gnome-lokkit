# gnome-lokkit
# Copyright (C) 2001 Free Software Foundation, Inc.
# Yukihiro Nakai <nakai@gnome.gr.jp>, 2001.
# Akira TAGOH <tagoh@gnome.gr.jp>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-lokkit CVS-20011220\n"
"POT-Creation-Date: 2001-12-20 19:13+0900\n"
"PO-Revision-Date: 2001-12-20 22:47+0900\n"
"Last-Translator: Akira TAGOH <tagoh@gnome.gr.jp>\n"
"Language-Team: Japanese <translation@gnome.gr.jp>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=euc-jp\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/gnome.c:33 src/newt.c:601
msgid ""
"Write Red Hat style /etc/sysconfig/ipchains rules, instead of a shell script"
msgstr "シェルスクリプトの代わりにRed Hatスタイルの /etc/sysconfig/ipchains ルールを書く"

#: src/gnome.c:38
msgid ""
"Lokkit is a tool to provide firewalling for the average\n"
"Linux end user. Instead of having to configure firewall\n"
"rules the Lokkit program asks a small number of simple\n"
"questions and writes a firewall rule set for you.\n"
"\n"
"Lokkit is not designed to configure arbitary firewalls.\n"
"To make it simple to understand it is solely designed to\n"
"handle typical dialup user and cable modem setups.\n"
"\n"
"It is not the equal of an expert firewall designer."
msgstr ""
"Lokkitは普通のLinuxエンドユーザのためのファイアウォール\n"
"設定ツールです。ファイアウォールのルールを設定するかわりに\n"
"Lokkitはいくつかの単純な問題を質問してファイアウォールの\n"
"ルールセットを作成します。\n"
"\n"
"Lokkitはどんなファイアウォールでも設定できるようには作られて\n"
"いません。理解しやすいように、典型的なダイアルアップユーザと\n"
"ケーブルモデムの設定を扱うように設計されています。\n"
"\n"
"プロのためのファイアウォール設計ツールではありません。"

#: src/gnome.c:48
msgid ""
"Lokkit is about to activate firewalling on this system.\n"
"It is strongly recommended you only activate the \n"
"firewalling facilities while you have direct access \n"
"to the machine, as you may not be able to get access \n"
"to deactivate them remotely.\n"
msgstr ""
"Lokkitはシステムのファイアウォールを有効にしました。\n"
"マシンに直接アクセスしている間だけファイアウォールを有効\n"
"にすることを強く推奨します。リモートからアクセスして無効に\n"
"するようなことはできないでしょうから。\n"

#: src/gnome.c:105 src/gnome.c:499
msgid "Configure Firewalling"
msgstr "ファイアウォールの設定"

#: src/gnome.c:135
msgid "Activate The Firewall"
msgstr "ファイアウォールを有効にする"

#. Error dialog
#: src/gnome.c:434
msgid "Unable to write firewall rules"
msgstr "ファイアウォールルールを書けません"

#: src/gnome.c:448
msgid "Mail services are enabled. Checking for relaying"
msgstr "メールサービスは有効になりました。転送をチェックしています"

#: src/gnome.c:476
msgid "You need to be the superuser to use this application."
msgstr "このアプリケーションを使うには、スーパーユーザになる必要があります。"

#: src/gnome.c:485
msgid ""
"You are about to override your old firewall configuration.\n"
" Are you sure you want to continue?\n"
msgstr ""
"以前のファイアウォール設定を上書きしようとしています。\n"
"本当に続けますか?\n"

#: src/gnome.c:507
msgid "Basic Configuration"
msgstr "基本設定"

#: src/gnome.c:508
msgid ""
"Lokkit can set up either a low or high security configuration. A high "
"security configuration blocks all incoming accesses but requires you use "
"specific ftp configurations and blocks irc dcc. The lower security option "
"blocks system services but is not so intrusive. "
msgstr ""
"Lokkitは高低両方のセキュリティに設定できます。高セキュリティ設定ではやってく"
"る すべてのアクセスをブロックし、特別なftp設定を要求してIRCのDCCをブロックし"
"ます 。低セキュリティ設定ではシステムサービスをブロックしますがそこまで強くは"
"ありま せん。"

#: src/gnome.c:513
msgid "High Security - more intrusive"
msgstr "高セキュリティ - 強力"

#: src/gnome.c:514
msgid "Low Security  - less intrusive"
msgstr "低セキュリティ- ちょっと強力"

#: src/gnome.c:515
msgid "Disable Firewall - no protection"
msgstr "ファイアウォール無効 - 保護なし"

#: src/gnome.c:523 src/gnome.c:535
msgid "Local Hosts"
msgstr "ローカルホスト"

#: src/gnome.c:524
msgid ""
"You seem to have an ethernet (eth0) attached to your machine. Shall I trust "
"the hosts that are connected via this card? If this connection is your link "
"to the internet (eg cable modem) you should say no."
msgstr ""
"マシンにイーサネット(eth0)があるようです。このカードで接続されている ホストを"
"信頼してもよろしいでしょうか? もしこの接続がインターネットへ のリンク(ケーブ"
"ルモデムなど)ならば、いいえを選択してください。"

#: src/gnome.c:528 src/gnome.c:540 src/gnome.c:550 src/gnome.c:560
#: src/gnome.c:573 src/gnome.c:584 src/gnome.c:596 src/gnome.c:608
msgid "   Yes   "
msgstr "   はい   "

#: src/gnome.c:529 src/gnome.c:541 src/gnome.c:551 src/gnome.c:561
#: src/gnome.c:574 src/gnome.c:585 src/gnome.c:597 src/gnome.c:609
msgid "   No    "
msgstr "   いいえ   "

#: src/gnome.c:536
msgid ""
"You seem to have a second ethernet (eth1) attached to your machine. Shall I "
"trust the hosts that are connected via this card? If this connection is your "
"link to the internet (eg cable modem) you should say no."
msgstr ""
"マシンにもう一つイーサネット(eth0)があるようです。このカードで接続されて いる"
"ホストを信頼してもよろしいでしょうか? もしこの接続がインターネットへ のリンク"
"(ケーブルモデムなど)ならば、いいえを選択してください。"

#: src/gnome.c:546 src/newt.c:421
msgid "DHCP"
msgstr "DHCP"

#: src/gnome.c:547
msgid ""
"Are you using DHCP on any of your interfaces? If you are then say yes and "
"the firewall rules will allow DHCP clients to negotiate addresses with your "
"provider."
msgstr ""
"インターフェースのどれかにDHCPを使っていますか? もしそうならはいと答えると"
"ファイアウォールルールがDHCPクライアントが サーバとアドレスの折衝をできるよう"
"になります。"

#: src/gnome.c:555
msgid "Services"
msgstr "サービス"

#: src/gnome.c:556
msgid ""
"You can enable access to some of the incoming services on your machine. "
"Before doing so you should check you are up to date with any errata from "
"your Linux vendor.\n"
"Do you wish to select services to enable.\n"
msgstr ""
"マシンにやって来るサービスのいくつかを有効にできます。その前にLinux ベンダの"
"errataに追従できているかどうかを確認してください。\n"
"有効にするサービスを選択してください。\n"

#: src/gnome.c:568
msgid "Web Server"
msgstr "Webサーバ"

#: src/gnome.c:569
msgid ""
"This allows people to access and view the web server on your computer. You "
"do not need to enable this to use the web server directly from the machine "
"itself for things like developing pages."
msgstr ""
"これはマシンにあるWebサーバにアクセスして見ることを許可します。 開発中のペー"
"ジなどマシンから直接見るだけのWebサーバならこれを 許可する必要はありません。"

#: src/gnome.c:580
msgid "Incoming Mail"
msgstr "入ってくるメール"

#: src/gnome.c:581
msgid ""
"Allow incoming SMTP mail delivery. You do not need to enable this if you "
"collect your mail from your ISP's server by POP3 or IMAP, or if you use a "
"tool such as fetchmail."
msgstr ""
"やって来るSMTPメール配信を許可します。メールをPOP3やIMAPでISPのサーバ から集"
"めている場合、またはfetchメールなどのツールを使っている場合は これを許可する"
"必要はありません。"

#: src/gnome.c:591
msgid "Secure Shell"
msgstr "セキュアシェル(ssh)"

#: src/gnome.c:592
msgid ""
"Allow incoming secure shell access. This is an encrypted way to talk to your "
"computer over the internet. You may also wish to read the ssh documentation "
"and set up a list of hosts permitted to connect."
msgstr ""
"やって来るsshのアクセスを許可します。これはインターネット越しにマシン と通信"
"する、暗号化された通信路です。sshのドキュメントを読んで接続を 許すホストの一"
"覧を設定したくなるかも知れません。"

#: src/gnome.c:603 src/newt.c:427
msgid "Telnet"
msgstr "telnet"

#: src/gnome.c:604
msgid ""
"Allow incoming telnet access. This allows access to the command line shell "
"of the machine from outside, if you have a username and password. It is not "
"encrypted and can therefore be spied upon. If possible avoid enabling this."
msgstr ""
"telnetアクセスを許可します。これは、ユーザ名とパスワードがあれば、 外部からマ"
"シンのコマンドラインシェルへのアクセスが許可されます。 暗号化されていないの"
"で、盗聴されているかも知れません。もし可能ならば これを許可するのは避けてくだ"
"さい。"

#: src/libinternet.c:82
msgid "Unable to allocate a socket"
msgstr "ソケットをallocateできません"

#: src/libinternet.c:88
msgid "Unable to set connection nonblocking"
msgstr "接続をnonblockにできません"

#. Time out
#: src/libinternet.c:113
msgid "Connection failed!"
msgstr "接続が失敗しました!"

#. Ermm.. ??
#: src/libinternet.c:118
msgid "Select failed!"
msgstr "selectが失敗しました!"

#: src/libinternet.c:140
#, c-format
msgid "Unable to resolve '%s'."
msgstr "'%s'を解決できません。"

#: src/libinternet.c:157
#, c-format
msgid "Unable to connect to '%s'."
msgstr "'%s'に接続できません。"

#: src/scanomatic.c:34
msgid "Mail relay testing"
msgstr "メール転送テスト"

#: src/scanomatic.c:37
msgid "Beginning mail test."
msgstr "メールテストの開始"

#: src/scanomatic.c:48
#, c-format
msgid "Mail relay check %p%% complete"
msgstr "メール転送チェック %p%% 完了"

#: src/scanomatic.c:145 src/scanomatic.c:159
msgid "Relay test result."
msgstr "転送テスト結果。"

#: src/scanomatic.c:170
msgid "Beginning mail tests"
msgstr "メールテストの開始"

#: src/scanomatic.c:201
msgid "EOF - passed"
msgstr "EOF - 通過"

#: src/scanomatic.c:204
msgid "Failed test sequence is: \n"
msgstr "失敗したテストシーケンスは: \n"

#: src/scanomatic.c:206
msgid "missing!\n"
msgstr "ありません!\n"

#: src/scanomatic.c:210
msgid "http://maps.vix.com/tsi/"
msgstr "http://maps.vix.com/tsi/"

#: src/scanomatic.c:224
msgid "Unexpected end of file"
msgstr "予期していないファイルのEOFです"

#: src/newt.c:64
msgid ""
"\n"
"Firewall Configuration\n"
"\n"
"   Red Hat Linux also offers you firewall protection\n"
"   for enhanced system security. A firewall sits\n"
"   between your computer and the network, and\n"
"   determines which resources on your computer remote\n"
"   users on the network are able to access. A\n"
"   properly configured firewall can greatly increase\n"
"   the out-of-the-box security of your system.\n"
"\n"
"   Choose the appropriate security level for your\n"
"   system.\n"
"\n"
"   High Security -- By choosing High Security, your\n"
"   system will not accept connections that are not\n"
"   explicitly defined by you. By default, only the\n"
"   following connections are allowed:\n"
"\n"
"     * DNS replies\n"
"     * DHCP -- so any network interfaces that use\n"
"       DHCP can be properly configured.\n"
"\n"
"   Using this High Security will not allow the\n"
"   following:\n"
"\n"
"     * Active mode FTP (Passive mode FTP, used by\n"
"       default in most clients, should work fine.)\n"
"     * IRC DCC file transfers\n"
"     * RealAudio(tm)\n"
"     * Remote X Window System clients\n"
"\n"
"   If you are connecting your system to the Internet,\n"
"   but do not plan to run a server, this is the\n"
"   safest choice. If additional services are needed,\n"
"   you can choose Customize to allow specific\n"
"   services through the firewall.\n"
"\n"
"   Medium Security -- Choosing Medium Security will\n"
"   not allow your system to have access to certain\n"
"   resources. By default, access to the following\n"
"   resources are not allowed:\n"
"\n"
"     * ports lower than 1023 -- these are the\n"
"       standard reserved ports, used by most system\n"
"       services, such as FTP, SSH, telnet, and HTTP.\n"
"     * NFS server port (2049)\n"
"     * the local X Window System display for remote X\n"
"       clients\n"
"     * the X Font server port (This is disabled by\n"
"       default in the font server.)\n"
"\n"
"   If you want to allow resources such as\n"
"   RealAudio(tm), while still blocking access to\n"
"   normal system services, choose Medium Security.\n"
"   You can choose Customize to allow specific\n"
"   services through the firewall.\n"
"\n"
"   No Firewall -- No firewall allows complete access\n"
"   and does no security checking. It is recommended\n"
"   that this only be selected if you are running on a\n"
"   trusted network (not the Internet), or if you plan\n"
"   to do more detailed firewall configuration later.\n"
"\n"
"   Choose Customize to add trusted devices or to\n"
"   allow additional incoming interfaces.\n"
msgstr ""
"\n"
"ファイアウォールの設定\n"
"\n"
"   Red Hat Linuxはシステムのセキュリティを向上させる\n"
"   ためファイアウォール保護を提供しています。\n"
"   ファイアウォールはコンピュータとネットワークの間に\n"
"   置かれ、ネットワーク上のリモートユーザがコンピュータ\n"
"   のリソースにアクセスすることができるか決定します。\n"
"   適切に設定されたファイアウォールはマシンの外側で\n"
"   システムのセキュリティを非常に強化することが\n"
"   できます。\n"
"\n"
"   システムに適切なセキュリティレベルを選択して\n"
"   ください\n"
"\n"
"   高セキュリティ -- これを選択することによって、\n"
"   あなたのシステムは明示的に定義されない接続は受けつけ\n"
"   ないでしょう。デフォルトによって以下の接続だけが\n"
"   許可されます:\n"
"\n"
"     * DNS 応答\n"
"     * DHCP -- DHCPを使ったネットワークインターフェース\n"
"       は適切に設定させることができます。\n"
"\n"
"   この高セキュリティを使うことは以下を許可しない\n"
"   でしょう:\n"
"\n"
"     * アクティブモード FTP (パッシブモード FTPは多くの\n"
"       クライアントで使うことができ、うまく動作するはずです)\n"
"     * IRC DCC ファイル転送\n"
"     * RealAudio(tm)\n"
"     * リモート X Window System クライアント\n"
"\n"
"   もしインターネットへシステムが接続していてサーバを\n"
"   走らせるプランがないようでしたら、これは安全な選択です。\n"
"   付加サービスが必要ならば、ファイアウォールを通過する\n"
"   固有のサービスを許可するためにカスタマイズを選択する\n"
"   こともできます。\n"
"\n"
"   中セキュリティ -- この選択は信頼できるリソースへアクセス\n"
"   するためにシステムを許可しないでしょう。デフォルトに\n"
"   よって以下のリソースへのアクセスは許可されません:\n"
"\n"
"     * 1023よりちいさいポート -- それらは標準で予約された\n"
"       ポートです。FTP, SSH, telnetやHTTPなどの多くの\n"
"       システムサービスによって使用されます。\n"
"     * NFSサーバポート (2049)\n"
"     * リモートXクライアントのためのローカルX Window System\n"
"       ディスプレイ\n"
"     * Xフォントサーバポート (これはフォントサーバ内で\n"
"       デフォルトで無効にされています)\n"
"\n"
"   もしRealAudio(tm)のようなリソースを許可したいくて、\n"
"   その上通常のシステムサービスのアクセスをブロックしたい\n"
"   なら、中セキュリティを選択します。\n"
"   ファイアウォールを通過する固有のサービスを許可する\n"
"   ためにカスタマイズを選択することもできます。\n"
"\n"
"   ファイアウォールなし -- これは完全にアクセスを\n"
"   許可してセキュリティチェックをしません。信頼された\n"
"   ネットワーク(インターネットではありません)で動作して\n"
"   いるか、あるいは後でファイアウォールの多くの詳細設定\n"
"   をするプランを立てているならば、これを選択させること\n"
"   を推奨できます。\n"
"\n"
"   信頼されたデバイスを追加するか、あるいは付加\n"
"   インターフェースを許可するためにカスタマイズを選択\n"
"   してください。\n"

#: src/newt.c:130
msgid ""
"\n"
"Firewall Customization\n"
"\n"
"   Choose which trusted devices and incoming services\n"
"   should be allowed for your network security\n"
"   settings.\n"
"\n"
"   Trusted Devices -- Checking these for any of your\n"
"   devices allows all traffic coming from that device\n"
"   to be allowed. For example, if you are running a\n"
"   local network, but are connecting to the Internet\n"
"   via a PPP dialup, you could check that eth0 is\n"
"   trusted to allow any traffic coming from your\n"
"   local network.\n"
"\n"
"   It is not recommended to enable this for devices\n"
"   that are connected to public networks, such as the\n"
"   Internet.\n"
"\n"
"   Allow Incoming -- Enabling these options allow the\n"
"   specified services to pass through the firewall.\n"
"   Note, during a workstation-class installation, the\n"
"   majority of these services are not present on the\n"
"   system.\n"
"\n"
"     * DHCP -- This allows DHCP queries and replies,\n"
"       and allows any network interfaces that use\n"
"       DHCP to determine their IP address. DHCP is\n"
"       normally enabled.\n"
"     * SSH -- Secure Shell (SSH) is a protocol for\n"
"       logging into and executing commands on remote\n"
"       machines. It provides secure encrypted\n"
"       communications. If you plan on accessing your\n"
"       machine remotely via SSH over a firewalled\n"
"       interface, enable this option. You need the\n"
"       openssh-server package installed for this\n"
"       option to be useful.\n"
"     * Telnet -- Telnet is a protocol for logging\n"
"       into remote machines. It is unencrypted, and\n"
"       provides little security from network snooping\n"
"       attacks. Enabling telnet is not recommended.\n"
"       You need the telnet-server package installed\n"
"       for this option to be useful.\n"
"     * WWW (HTTP) -- HTTP is the protocol used by\n"
"       Apache to serve Web pages. If you plan on\n"
"       making your Web server publicly available,\n"
"       enable this option. This option is not\n"
"       required for viewing pages locally or\n"
"       developing Web pages. You need the Apache\n"
"       package installed for this option to be\n"
"       useful.\n"
"     * Mail (SMTP) -- This allows incoming SMTP mail\n"
"       delivery. If you need to allow remote hosts to\n"
"       connect directly to your machine to deliver\n"
"       mail, enable this option. You do not need to\n"
"       enable this if you collect your mail from your\n"
"       ISP's server by POP3 or IMAP, or if you use a\n"
"       tool such as fetchmail. Note that an\n"
"       improperly configured SMTP server can allow\n"
"       remote machines to use your server to send\n"
"       spam.\n"
"     * FTP -- FTP is a protocol used for remote file\n"
"       transfer. If you plan on making your FTP\n"
"       server publicly available, enable this option.\n"
"       You need the wu-ftpd (and possibly anonftp)\n"
"       packages installed for this option to be\n"
"       useful.\n"
"     * Other ports -- You can specify that other\n"
"       ports not listed here be allowed through the\n"
"       firewall. The format to use is\n"
"       'port:protocol'. For example, if you wanted to\n"
"       allow IMAP access through your firewall, you\n"
"       can specify 'imap:tcp'. You can also specify\n"
"       numeric ports explicitly; to allow UDP packets\n"
"       on port 1234 through, specify '1234:udp'. To\n"
"       specify multiple ports, separate them by\n"
"       commas.\n"
msgstr ""
"\n"
"ファイアウォールのカスタマイズ\n"
"\n"
"   信頼するデバイスとネットワークセキュリティ設定の\n"
"   ために許可させる入力サービスを選択します。\n"
"\n"
"   信頼するデバイス -- 許可するためのそのデバイスから\n"
"   くるすべてのトラフィックをいくつかのデバイスが許可\n"
"   するためにそれらをチェックします。例えば、ローカル\n"
"   ネットワークで動作させていても、PPPダイアルアップ\n"
"   経由でインターネットに接続しているなら、eth0は\n"
"   ローカルネットワークからくるいくつかのトラフィック\n"
"   を許可するために信頼されるため、チェックできる\n"
"   のではないでしょうか。\n"
"\n"
"   インターネットのような公開されたネットワークへ接続\n"
"   させるデバイスのためにこれを有効にするのは推奨され\n"
"   ません。\n"
"\n"
"   許可する入力 -- ファイアウォールを通過させる特定の\n"
"   サービスを許可するためにこれらのオプションを有効に\n"
"   します。注意として、ワークステーションクラスの\n"
"   インストールでは、それらのサービスの大部分はシステム\n"
"   で提供されていません。\n"
"\n"
"     * DHCP -- これはDHCPのクエリや応答を許可し、そして\n"
"       それらのアドレスを決定するためにDHCPを使った\n"
"       ネットワークインターフェースを許可します。DHCP\n"
"       は通常有効にされています。\n"
"     * SSH -- セキュアシェル(SSH)はリモートマシンへの\n"
"       ログインとコマンドの実行をするためのプロトコルです。\n"
"       それは暗号化された安全な通信を提供します。\n"
"       もしファイアウォールに保護されたインターフェース\n"
"       を通過してSSH経由でリモートマシンへアクセスする\n"
"       プランがあるなら、このオプションを有効にします。\n"
"       このオプションのためにopenssh-serverパッケージを\n"
"       インストールする必要があります。\n"
"     * Telnet -- Telnetはリモートマシンへログインする\n"
"       ためのプロトコルです。それは暗号化は行いません。\n"
"       そしてnetwork snooping attackからちょっとした\n"
"       セキュリティを提供します。telnetを有効にするのは\n"
"       推奨されません。このオプションのために\n"
"       telnet-serverパッケージをインストールする必要が\n"
"       あります。\n"
"     * WWW (HTTP) -- HTTPはウェブページを役立たせるため\n"
"       にApacheによって使われるプロトコルです。もし\n"
"       ウェブサーバを公に利用させるプランがあるなら、\n"
"       このオプションを有効にします。このオプションは\n"
"       ローカルページを表示するかあるいはウェブページ\n"
"       の開発には必要ありません。このオプションのために\n"
"       Apacheパッケージをインストールする必要があります。\n"
"     * メール (SMTP) -- これはメールを配送するために\n"
"       あなたのマシンに直接リモートホストが接続することを\n"
"       許可する必要がある場合に、このオプションを有効に\n"
"       します。もしPOP3やIMAPによってISPのサーバから\n"
"       メールを受け取るかあるいはfetchmailのようなツール\n"
"       を使うなら、これを有効にする必要はありません。\n"
"       spamを送るためにリモートマシンがあなたのサーバを\n"
"       使用するのを不当に設定されたSMTPサーバは許可する\n"
"       ことができることに注意してください。\n"
"     * FTP -- FTPはリモートファイルの転送のために使われる\n"
"       プロトコルです。もし公にFTPサーバを利用させる\n"
"       プランがあるなら、このオプションを有効にします。\n"
"       このオプションのためにwu-ftpd(そして可能ならば\n"
"       anonftp)をインストールする必要があります。\n"
"     * その他のポート -- ファイアウォールを通過させる\n"
"       ことを許可されたリストにはないその他のポートを\n"
"       指定することができます。使用するためのフォーマット\n"
"       は'ポート:プロトコル'です。例えば、IMAPアクセスで\n"
"       ファイアウォールを通過させたい場合には'imap:tcp'\n"
"       を指定することができます。また明示的にポート番号\n"
"       を指定することもできます。ポート234を通じてUDP\n"
"       パケットを許可するために'1234:udp'を指定します。\n"
"       複数のポートを指定するためには、コンマによって\n"
"       それらを区切ります。\n"

#: src/newt.c:210 src/newt.c:479
msgid "Firewall Configuration"
msgstr "ファイアウォールの設定"

#: src/newt.c:210 src/newt.c:212 src/newt.c:498
msgid "Ok"
msgstr "OK"

#: src/newt.c:212
msgid "Firewall Customization"
msgstr "ファイアウォールのカスタマイズ"

#: src/newt.c:306 src/newt.c:501
msgid "OK"
msgstr "OK"

#: src/newt.c:306
msgid "Customize"
msgstr "カスタマイズ"

#: src/newt.c:306
msgid "Cancel"
msgstr "キャンセル"

#: src/newt.c:309
msgid ""
"A firewall protects against unauthorized network intrusions. High security "
"blocks all incoming accesses. Medium blocks access to system services (such "
"as telnet or printing), but allows other connections. No firewall allows all "
"connections and is not recommended. "
msgstr "ファイアウォールは許可されないネットワークの侵入に逆らって保護します。高セキュリティはすべての入力アクセスをブロックします。中セキュリティはシステムサービス(telnetや印刷のような)へのアクセスをブロックし、その他の接続を許可します。ファイアウォールなしはすべての接続を許可しますが推奨されません。"

#: src/newt.c:323
msgid "Security Level:"
msgstr "セキュリティレベル:"

#: src/newt.c:325
msgid "High"
msgstr "高"

#: src/newt.c:328
msgid "Medium"
msgstr "中"

#: src/newt.c:331
msgid "No firewall"
msgstr "ファイアウォールなし"

#: src/newt.c:355
msgid "Trusted Devices:"
msgstr "信頼されたデバイス:"

#: src/newt.c:386
msgid "Allow incoming:"
msgstr "許可する入力:"

#: src/newt.c:424
msgid "SSH"
msgstr "SSH"

#: src/newt.c:430
msgid "WWW (HTTP)"
msgstr "WWW (HTTP)"

#: src/newt.c:433
msgid "Mail (SMTP)"
msgstr "メール (SMTP)"

#: src/newt.c:436
msgid "FTP"
msgstr "FTP"

#: src/newt.c:441
msgid "Other ports"
msgstr "その他のポート"

#: src/newt.c:467
msgid ""
" <Tab>/<Alt-Tab> between elements   |   <Space> selects  |   <F12> next "
"screen"
msgstr " <TAB>/<Alt-TAB> エレメント移動     |   <SPACE> 選択     |   <F12> 次の画面"

#: src/newt.c:470
#, c-format
msgid "lokkit %s                     (C) 2001 Red Hat, Inc."
msgstr "lokkit %s                     (C) 2001 Red Hat, Inc."

#: src/newt.c:498
msgid "Invalid Choice"
msgstr "無効な選択です"

#: src/newt.c:499
msgid "You cannot customize a disabled firewall."
msgstr "ファイアウォールを無効にしたカスタマイズはできません"

#: src/newt.c:506
msgid ""
"You can customize your firewall in two ways. First, you can select to allow "
"all traffic from certain network interfaces. Second, you can allow certain "
"protocols explicitly through the firewall. Specify additional ports in the "
"form 'service:protocol', such as 'imap:tcp'. "
msgstr "ふたつの方法でファイアウォールをカスタマイズすることができます。はじめに確実なネットワークインターフェースからのすべてのトラフィックを許可するために選択することです。次に明示的にファイアウォールを通過させる確実なプロトコルを許可させます。'imap:tcp'のような'サービス:プロトコル'の形式で付加ポートを指定します。"

#: src/newt.c:525
msgid "Firewall Configuration - Customize"
msgstr "ファイアウォール設定 - カスタマイズ"

#: src/newt.c:605
msgid "Run noninteractively; process only command-line arguments"
msgstr "非対話的に実行; コマンドラインの引数だけを処理する"

#: src/newt.c:608
msgid "Configure firewall but do not activate it"
msgstr "ファイアウォールを設定しても作動させない"

#: src/newt.c:612
msgid "Enable 'high' security level (default)"
msgstr "'高' セキュリティレベルを有効 (デフォルト)"

#: src/newt.c:616
msgid "Enable 'medium' security level"
msgstr "'中' セキュリティレベルを有効"

#: src/newt.c:620
msgid "Disable firewall"
msgstr "ファイアウォールを無効"

#: src/newt.c:624
msgid "Allow DHCP through the firewall"
msgstr "DHCPのファイアウォール通過を許可"

#: src/newt.c:628
msgid "Allow specific ports through the firewall"
msgstr "特定のポートのファイアウォール通過を許可"

#: src/newt.c:629
msgid "port:protocol (e.g, ssh:tcp)"
msgstr "ポート:プロトコル (例 ssh:tcp)"

#: src/newt.c:632
msgid "Allow all traffic on the specified device"
msgstr "特定のデバイス上ですべてのトラフィックを許可"

#: src/newt.c:633
msgid "device to trust"
msgstr "信頼するデバイス"

#: src/newt.c:640
msgid ""
"\n"
"ERROR - You must be root to run lokkit.\n"
msgstr "\nエラー - lokkitをrootで動作させなければなりません\n"

#: src/newt.c:659
msgid ""
"ERROR - only one of 'high', 'medium', and 'disabled' may be specified.\n"
msgstr "エラー - '高', '中', 'なし'のひとつだけを指定させることができます\n"
