%define nam	Lokkit
%define ver	0.50
%define rel	1

%define prefix		/usr
%define sysconfdir	/etc

Summary:	Firewall configuration application for an average end user.
Name:		%nam
Version:	%ver
Release:	%rel
Copyright:	GPL
Group:		Networking/Utilities
Source:		ftp://ftp.linux.org.uk/pub/linux/alan/%{nam}-%{ver}.tar.gz
URL:		http://www.linux.org.uk/apps/lokkit.shtml
BuildRoot:	/var/tmp/%{nam}-%{ver}-root
Docdir:		%{prefix}/doc

Requires:	rp3
Requires: 	gnome-core >= 1.0.0

%description
Lokkit is an attempt to provide firewalling for the average Linux end user.
Instead of having to configure firewall rules the Lokkit program asks a
small number of simple questions and writes a firewall rule set for you.

Lokkit is not designed to configure arbitary firewalls. To make it simple to
understand it is solely designed to handle typical dialup user and cable
modem setups. It is not the answer to a complex firewall configuration, and
it is not the equal of an expert firewall designer.

%changelog

* Mon Feb 21 2000  Julian Missig  <julian@linuxpower.org>

- First version of the spec file. It seems to work.

%prep
%setup -q

%build
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh	\
	--prefix=%{prefix} --sysconfdir=%{sysconfdir}
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure	\
	--prefix=%{prefix} --sysconfdir=%{sysconfdir}
fi

make

%install
rm -rf $RPM_BUILD_ROOT
make install \
	prefix=$RPM_BUILD_ROOT%{prefix} \
	sysconfdir=$RPM_BUILD_ROOT%{sysconfdir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{prefix}/sbin/*
%{prefix}/share/gnome/apps/*
%{prefix}/share/gnome/help/*
%{prefix}/share/pixmaps/*
%{prefix}/share/locale/*/*/*

